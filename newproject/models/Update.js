import { Link } from '../systems/Config';
import { ToastAndroid } from 'react-native';

export var responseUpdate;

export function updateProduct ( product_id, name_product, category, price, detail ) {

    return new Promise(( resolve, reject ) => {
        
        fetch( Link + 'update/updateProduct', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                product_id: product_id,
                name_product: name_product,
                category: category,
                price: price,
                detail: detail
            }),
        })
        .then(( response ) => response.json())
        .then(( json ) => {
            responseUpdate = json;
            resolve( true );
        })
        .catch(( error ) => {
            ToastAndroid.show( "No Internet Connection", ToastAndroid.SHORT );
            reject( true );
        });
    });
}