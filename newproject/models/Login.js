import { Link } from '../systems/Config';
import { ToastAndroid } from 'react-native';

export var responseLogin, responseRegister;

export function doLogin ( email, password ) {

    return new Promise(( resolve, reject ) => {
        
        fetch( Link + 'auth/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email,
                password: password,
            }),
        })
        .then(( response ) => response.json())
        .then(( json ) => {
            responseLogin = json;
            resolve( true );
        })
        .catch(( error ) => {
            ToastAndroid.show( "Tidak Ada Koneksi Internet", ToastAndroid.SHORT );
            reject( true );
        });
    });
}

export function doRegister ( fullname, email, password ) {

    return new Promise(( resolve, reject ) => {
        
        fetch( Link + 'auth/reg', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                fullname: fullname,
                email: email,
                password: password,
            }),
        })
        .then(( response ) => response.json())
        .then(( json ) => {
            responseRegister = json;
            resolve( true );
        })
        .catch(( error ) => {
            ToastAndroid.show( "No Internet Connection", ToastAndroid.SHORT );
            reject( true );
        });
    });
}