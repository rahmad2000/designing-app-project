import { Link } from '../systems/Config';
import { ToastAndroid } from 'react-native';

export var responseCart, responseDelete, responseAddCart, responseCheckout, responseTotalPrice;

export function getCart ( user_id ) {

    return new Promise(( resolve, reject ) => {
        
        fetch( Link + 'view/getcart', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                user_id: user_id,
            }),
        })
        .then(( response ) => response.json())
        .then(( json ) => {
            responseCart = json;
            resolve( true );
        })
        .catch(( error ) => {
            ToastAndroid.show( "Tidak Ada Koneksi Internet", ToastAndroid.SHORT );
            reject( true );
        });
    });
}

export function deleteCart ( cart_id ) {

    return new Promise(( resolve, reject ) => {
        
        fetch( Link + 'delete/deletecart', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                cart_id: cart_id,
            }),
        })
        .then(( response ) => response.json())
        .then(( json ) => {
            responseDelete = json;
            resolve( true );
        })
        .catch(( error ) => {
            ToastAndroid.show( "Tidak Ada Koneksi Internet", ToastAndroid.SHORT );
            reject( true );
        });
    });
}

export function addToCart ( user_id, product_id, count_product, total_price, colour, size ) {

    return new Promise(( resolve, reject ) => {
        
        fetch( Link + 'add/cart', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                user_id: user_id,
                product_id: product_id,
                count_product: count_product,
                total_price: total_price,
                colour: colour,
                size: size
            }),
        })
        .then(( response ) => response.json())
        .then(( json ) => {
            responseAddCart = json;
            resolve( true );
        })
        .catch(( error ) => {
            ToastAndroid.show( "Tidak Ada Koneksi Internet", ToastAndroid.SHORT );
            reject( true );
        });
    });
}

export function checkout ( user_id, address, date ) {

    return new Promise(( resolve, reject ) => {
        
        fetch( Link + 'add/checkout', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                user_id: user_id,
                address: address,
                date: date
            }),
        })
        .then(( response ) => response.json())
        .then(( json ) => {
            responseCheckout = json;
            resolve( true );
        })
        .catch(( error ) => {
            ToastAndroid.show( "No Internet Connection", ToastAndroid.SHORT );
            reject( true );
        });
    });
}

export function totalPrice ( user_id ) {

    return new Promise(( resolve, reject ) => {
        
        fetch( Link + 'view/totalprice', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                user_id: user_id,
            }),
        })
        .then(( response ) => response.json())
        .then(( json ) => {
            responseTotalPrice = json;
            resolve( true );
        })
        .catch(( error ) => {
            ToastAndroid.show( "Tidak Ada Koneksi Internet", ToastAndroid.SHORT );
            reject( true );
        });
    });
}