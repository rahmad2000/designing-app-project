import { Link } from '../systems/Config';
import { ToastAndroid } from 'react-native';

export var responseDesign, responseProduct;

export function uploadDesign ( formData ) {

    return new Promise(( resolve, reject ) => {
        
        fetch( Link + 'add/design', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: formData
        })
        .then(( response ) => response.json())
        .then(( json ) => {
            responseDesign = json;
            resolve( true );
        })
        .catch(( error ) => {
            ToastAndroid.show( "No Internet Connection", ToastAndroid.SHORT );
            reject( true );
        });
    });
}

export function uploadProduct ( userId, productName, productImage, productCategory, productPrice, productDetail, productSize, designId ) {

    return new Promise(( resolve, reject ) => {
        
        fetch( Link + 'add/product', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                user_id: userId, 
                name_product: productName,
                image: productImage,
                category: productCategory,
                price: productPrice,
                detail: productDetail,
                size: productSize,
                design_id: designId
            })
        })
        .then(( response ) => response.json())
        .then(( json ) => {
            responseProduct = json;
            resolve( true );
        })
        .catch(( error ) => {
            ToastAndroid.show( "No Internet Connection", ToastAndroid.SHORT );
            reject( true );
        });
    });
}