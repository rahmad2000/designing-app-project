import { Link } from '../systems/Config';
import { ToastAndroid } from 'react-native';

export var responseFilter;

export function getFilter ( category ) {

    return new Promise(( resolve, reject ) => {
        
        fetch( Link + 'view/filter', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                category: category,
            }),
        })
        .then(( response ) => response.json())
        .then(( json ) => {
            responseFilter = json;
            resolve( true );
        })
        .catch(( error ) => {
            ToastAndroid.show( "No Internet Connection", ToastAndroid.SHORT );
            reject( true );
        });
    });
}