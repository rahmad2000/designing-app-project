import { Link } from '../systems/Config';
import { ToastAndroid } from 'react-native';

export var responseProduct;

export function getProduct () {

    return new Promise(( resolve, reject ) => {
        
        fetch( Link + 'view/getproduct', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        })
        .then(( response ) => response.json())
        .then(( json ) => {
            responseProduct = json;
            resolve( true );
        })
        .catch(( error ) => {
            ToastAndroid.show( "No Internet Connection", ToastAndroid.SHORT );
            reject( true );
        });
    });
}