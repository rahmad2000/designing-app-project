import React from 'react';
import { AsyncStorage, Alert, ToastAndroid } from 'react-native';
import DesignListScreenView from "../views/DesignListScreenView";
import { getDesign, responseDesign } from "../models/Admin";

export default class DesignListScreen extends React.Component {
    
    constructor(props) {

        super(props);

        this.state = {
            modalVisible : false,
            refreshing : false,
            search: ""
        }

        this.method = {
            onRefresh: this.onRefresh.bind(this),
            logout: this.logout.bind(this),
            onChangeSearch: this._onChangeSearch.bind(this),
            onSubmitSearch: this._onSubmitSearch.bind(this),
            itemDesignPress: this._itemDesignPress.bind(this)
        }

        this.content = [];

    }

    onRefresh = () => {
        this.setState({refreshing: true});
        this.designHandler().then(() => {
          this.setState({refreshing: false, search: ""});
        }).catch(( error ) => {
            this.setState({refreshing: false, search: ""});
        });
    }

    onLoading = () => {
        this.setState({modalVisible: true});
        this.designHandler().then(() => {
          this.setState({modalVisible: false});
        }).catch(( error ) => {
            this.setState({modalVisible: false});
        });
    }

    logout = () => {
        Alert.alert("Logout", "Anda yakin ingin logout?", [
            {text: 'Ya', onPress: () => {
                AsyncStorage.clear(() => {
                    this.props.navigation.push("Login");
                })
            }},
            {text: 'Tidak', onPress: () => {
                this.props.navigation.push("Admin");
            }},
        ],{cancelable: false})
    }

    componentWillMount = () => {
        this.designHandler();
        this.onLoading();
    }

    designHandler = async () => {
        await getDesign()
        .then(() => {
            this.setState({
                dataComplete: responseDesign.data,
                data: responseDesign.data
            });
        });
    }

    _onChangeSearch = (value) =>{
        this.setState({
            search: value
        })
    }

    _onSubmitSearch = (search) =>{
        let dataSearch = this.state.dataComplete.filter((str) => {return str.name_design.toLowerCase().indexOf(search.toLowerCase()) !== -1})
        // let dataSearch = this.state.dataComplete.filter( (data) => data.name_design == search )
        if(search == ""){
            this.designHandler()
        }else if(dataSearch.length < 1){
            ToastAndroid.show("Data not found for "+search, ToastAndroid.SHORT)
        }else{
             this.setState({
                data: dataSearch
            })
        }
       
    }

    _itemDesignPress = (params) =>{
        this.props.navigation.push("AdminDesignDetail", params)
    }

    render = () => {
        return <DesignListScreenView state={this.state} method={this.method} />
    }

}