import React from 'react';
import { AsyncStorage, Alert, ToastAndroid } from 'react-native';
import AdminProductScreenView from "../views/AdminProductScreenView";
import { getProduct, responseProduct } from "../models/Admin";

export default class AdminProductScreen extends React.Component {
    
    constructor(props) {

        super(props);

        this.state = {
            modalVisible : false,
            refreshing : false,
            search: ""
        }

        this.method = {
            onRefresh: this.onRefresh.bind(this),
            logout: this.logout.bind(this),
            onChangeSearch: this._onChangeSearch.bind(this),
            onSubmitSearch: this._onSubmitSearch.bind(this),
            itemProductPress: this._itemProductPress.bind(this)
        }

        this.content = [];

    }

    onRefresh = () => {
        this.setState({refreshing: true});
        this.productHandler().then(() => {
          this.setState({refreshing: false, search: ""});
        }).catch(( error ) => {
            this.setState({refreshing: false, search: ""});
        });
    }

    onLoading = () => {
        this.setState({modalVisible: true});
        this.productHandler().then(() => {
          this.setState({modalVisible: false});
        }).catch(( error ) => {
            this.setState({modalVisible: false});
        });
    }

    logout = () => {
        Alert.alert("Logout", "Anda yakin ingin logout?", [
            {text: 'Ya', onPress: () => {
                AsyncStorage.clear(() => {
                    this.props.navigation.push("Login");
                })
            }},
            {text: 'Tidak', onPress: () => {
                this.props.navigation.push("AdminProduct");
            }},
        ],{cancelable: false})
    }

    componentWillMount = () => {
        this.productHandler();
        this.onLoading();
    }

    productHandler = async () => {
        await getProduct()
        .then(() => {
            this.setState({
                dataComplete: responseProduct.data,
                data: responseProduct.data
            });
        });
    }

    _onChangeSearch = (value) =>{
        this.setState({
            search: value
        })
    }

    _onSubmitSearch = (search) =>{
        let dataSearch = this.state.dataComplete.filter( (data) => data.name_product == search );
        if(search == ""){
            this.productHandler()
        }else if(dataSearch.length < 1){
            ToastAndroid.show("Data not found for "+search, ToastAndroid.SHORT)
        }else{
             this.setState({
                data: dataSearch
            })
        }
       
    }

    _itemProductPress = (params) =>{
        this.props.navigation.push("EditProduct", params)
    }

    render = () => {
        return <AdminProductScreenView state={this.state} method={this.method} />
    }

}