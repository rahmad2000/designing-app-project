import React from 'react';
import { ToastAndroid, Alert, AsyncStorage } from 'react-native';
import CheckoutScreenView from "../views/CheckoutScreenView";
import { getCart, responseCart, totalPrice, responseTotalPrice, checkout, responseCheckout } from "../models/Cart";

export default class CheckoutScreen extends React.Component {
    
    constructor(props) {

        super(props);

        this.state = {
            user_id: this.props.navigation.getParam('userId', ''),
            modalVisible : false,
            refreshing : false,
            date: ''+new Date().getDate()+' - '+new Date().getMonth()+' - '+new Date().getFullYear()+'',
        }

        this.method = {
            address: this._address.bind(this),
            transaction: this._transaction.bind(this)
        }

        this.content = [];

    }

    onLoading = () => {
        this.setState({modalVisible: true});
        this.cartHandler().then(() => {
          this.setState({modalVisible: false});
        }).catch(( error ) => {
            this.setState({modalVisible: false});
        });
    }

    componentWillMount = () => {
        this._subscribe = this.props.navigation.addListener('didFocus', () => {
            this.onLoading();
            this.totalHandler()
        })
    }
    
    totalHandler = async () => {
        await totalPrice(this.state.user_id)
        .then(() => {
            if(responseTotalPrice.status){
                this.setState({
                    price: responseTotalPrice.data.total_price,
                });
            }else{
                ToastAndroid.show("get data failed", ToastAndroid.SHORT);
            }
        });
    }

    cartHandler = async () => {
        await getCart(this.state.user_id)
        .then(() => {
            if(responseCart){
                this.setState({
                    data: responseCart.data,
                });
            }else{
                ToastAndroid.show("get data failed", ToastAndroid.SHORT);
            }
        });
    }

    _address = (value) =>{
        this.setState({
            address: value
        })
    }

    _transaction = async () => {
        await checkout( this.state.user_id, this.state.address, this.state.date )
        .then(() => {
            if(responseCheckout){
                ToastAndroid.show("Transaction Success", ToastAndroid.SHORT);
                this.props.navigation.push('Drawer')
            }else{
                ToastAndroid.show("Failed", ToastAndroid.SHORT);
            }
        });
    }

    render = () => {
        return <CheckoutScreenView state={this.state} method={this.method} />
    }

}