import React, { Component } from 'react';
import { ToastAndroid, AsyncStorage, BackHandler } from 'react-native';
import LoginScreenView from "../views/LoginScreenView";
import { doLogin, responseLogin, doRegister, responseRegister } from "../models/Login";

export default class LoginScreen extends React.Component {
    
    constructor(props) {

        super(props);

        this.state = {
            content:'1',
            user_id : '',
            nameLogin : '',
            emailLogin : '',
            passwordLogin : '',
            level : '',
            nameRegister : '',
            emailRegister:'',
            passwordRegister:'',
            onLoading: false
        }

        this.method ={
            setLogin:this.setLogin.bind(this),
            setRegister: this.setRegister.bind(this),
            onLogin: this.loginHandler.bind(this),
            onRegister: this.registerHandler.bind(this)
        }

        this.content = [];

    }

    componentDidMount = () => {
        BackHandler.addEventListener('hardwareBackPress', this.handlerBack);
    }

    componentWillUnmount = () => {
        BackHandler.removeEventListener('hardwareBackPress', this.handlerBack);
    }

    _onBackPress = () => {
        BackHandler.removeEventListener('hardwareBackPress', this.handlerBack)
    }

    handlerBack = () => {
        BackHandler.exitApp()
        return true
    }

    setLogin = () => {
        this.setState({
            content:'1'
        })
    }

    setRegister = () => {
        this.setState({
            content:'2'
        })
    }

    loginHandler = async () => {
        if( this.state.emailLogin && this.state.passwordLogin != "" ){
            this.setState({
                onLoading : true
            })
            await doLogin(
                this.state.emailLogin,
                this.state.passwordLogin,
            )
            .then(() => {
                this.setState({
                    onLoading : false
                })
                if( responseLogin.status ){
                    this.setState({
                        user_id : responseLogin.data.user_id,
                        fullname: responseLogin.data.fullname,
                        level: responseLogin.data.level,
                    });
                
                    AsyncStorage.setItem("user_id", this.state.user_id)
                    AsyncStorage.setItem("fullname", this.state.fullname)
                    AsyncStorage.setItem("email", this.state.emailLogin)
                    AsyncStorage.setItem("password", this.state.passwordLogin)
                    AsyncStorage.setItem("level", this.state.level)
                    
                    if(responseLogin.data.level == "admin"){
                        this.props.navigation.push("Admin");
                        ToastAndroid.show( "Welcome Admin "+this.state.fullname, ToastAndroid.SHORT );
                    }else{
                        this.props.navigation.push("Drawer");
                        ToastAndroid.show( "Welcome "+this.state.fullname, ToastAndroid.SHORT );
                    }
                }else{
                    ToastAndroid.show( "Incorrect Email or Password", ToastAndroid.SHORT );
                }
            }).catch((error) => {
                this.setState({
                    onLoading : false
                })
            });
        }else{
            ToastAndroid.show( "Please complete all fields", ToastAndroid.SHORT );
        }
        
    }

    registerHandler = async () => {
        if( this.state.nameRegister && this.state.emailRegister && this.state.passwordRegister != "" ){
            this.setState({
                onLoading : true
            })
            await doRegister(
                this.state.nameRegister,
                this.state.emailRegister,
                this.state.passwordRegister,
            )
            .then(() => {
                this.setState({
                    onLoading : false
                })
                if( responseRegister.status ){
                    this.setLogin();
                    ToastAndroid.show( "Register Success, Please Login ", ToastAndroid.SHORT );
                }else{
                    ToastAndroid.show( "Register Failed", ToastAndroid.SHORT );
                }
            }).catch((error) =>{
                this.setState({
                    onLoading : false
                })
            });
        }else{
            ToastAndroid.show( "Please complete all fields", ToastAndroid.SHORT );
        }
        
    }

    render = () => {
        return <LoginScreenView
        emailValueLogin={this.state.emailLogin}
        passwordValueLogin={this.state.passwordLogin}
        onChangeEmailLogin={(emailLogin) => this.setState({ emailLogin })}
        onChangePasswordLogin={(passwordLogin) => this.setState({ passwordLogin })}

        nameValueRegister = {this.state.nameRegister}
        emailValueRegister = {this.state.emailRegister}
        passwordValueRegister = {this.state.passwordRegister}
        onChangeNameRegister = {(nameRegister) => this.setState({ nameRegister })}
        onChangeEmailRegister = {(emailRegister) => this.setState({ emailRegister })}
        onChangePasswordRegister = {(passwordRegister) => this.setState({ passwordRegister })}

        method = {this.method}
        state = {this.state}
        onPressRegisterOffice={()=> this.props.navigation.push('Register')}
        />
    }

}