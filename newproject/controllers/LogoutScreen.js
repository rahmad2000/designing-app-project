import React, { Component } from 'react';
import { AsyncStorage, Alert } from 'react-native';
import LogoutScreenView from '../views/LogoutScreenView'

export default class LogoutScreen extends React.Component {
    
    constructor(props) {

        super(props);

        this.state = {}
        

    }

    componentWillMount = () => {
        Alert.alert("Logout", "Anda yakin ingin logout?", [
            {text: 'Ya', onPress: () => {
                AsyncStorage.clear(() => {
                    this.props.navigation.push("Login");
                })
            }},
            {text: 'Tidak', onPress: () => {
                this.props.navigation.push("Drawer");
            }},
        ],{cancelable: false})
    }

    render = () => {
        return <LogoutScreenView />
    }

}