import React from 'react';
import CategoryScreenView from "../views/CategoryScreenView";

export default class CategoryScreen extends React.Component {
    
    constructor(props) {

        super(props);

        this.state = {
        }

        this.method = {
            openDrawer: this.openDrawer.bind(this),
            onCategoryPress: this._onCategoryPress.bind(this)
        }

        this.content = [];

    }

    openDrawer = () => {
        this.props.navigation.openDrawer();
    }

    _onCategoryPress = (categoryName) =>{
        this.props.navigation.push("ItemCategory", {category: categoryName})
    }

    render = () => {
        return <CategoryScreenView state={this.state} method={this.method} />
    }

}