import React, { Component } from 'react';
import { Alert, ToastAndroid, BackHandler, Dimensions, PixelRatio, AsyncStorage } from 'react-native'
import DesignScreenView from "../views/DesignScreenView";
import CameraRoll from "@react-native-community/cameraroll";
import { PermissionsAndroid } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import { captureRef } from 'react-native-view-shot';
import { uploadDesign, responseDesign } from '../models/Upload';

export default class DesignScreen extends React.Component {
    
    constructor(props) {

        super(props);

        this.state = {
            bgColor: "#FFFFFF",
            textColor: "#000000",
            fullscreen: false,
            colorPickerVisible: false,
            textSettingVisible: false,
            textData: [],
            imageData: [],
            selectedText: null,
            textValue: "",
            imagePickModal: false,
            imageSettingVisible: false,
            fontSetVisible: false,
            selectedImage: null,
            nameVisible: false,
        }

        this.method = {
            colorPickerVisible: this._colorPickerVisible.bind(this),
            textSettingVisible: this._textSettingVisible.bind(this),
            setFullscreen: this._setFullscreen.bind(this),
            addText: this._addText.bind(this),
            bgColorChange: this._bgColorChange.bind(this),
            textEdit: this._textEdit.bind(this),
            onChangeText: this._onChangeText.bind(this),
            textSizeEdit: this._textSizeEdit.bind(this),
            onChangeFontSize: this._onChangeFontSize.bind(this),
            textColorChange: this._textColorChange.bind(this),
            saveImage: this._saveImage.bind(this),
            imagePicker: this._imagePicker.bind(this),
            imagePickModal: this._imagePickModal.bind(this),
            imageSettingVisible: this._imageSettingVisible.bind(this),
            imageWidthEdit: this._imageWidthEdit.bind(this),
            imageHeightEdit: this._imageHeightEdit.bind(this),
            onChangeImageHeight: this._onChangeImageHeight.bind(this),
            onChangeImageWidth: this._onChangeImageWidth.bind(this),
            layerForward: this._layerForward.bind(this),
            layerBack: this._layerBack.bind(this),
            layerToFront: this._layerToFront.bind(this),
            layerToBack: this._layerToBack.bind(this),
            deleteText: this._deleteText.bind(this),
            deleteImage: this._deleteImage.bind(this),
            fontSetting: this._fontSetting.bind(this),
            setSelectedFont: this._setSelectedFont.bind(this),
            saveFont: this._saveFont.bind(this),
            nameVisible: this._nameVisible.bind(this),
            onChangeName: this._onChangeName.bind(this),
        }

        this.content = [];
    }

    componentWillMount = () => {
        this._subscribe = this.props.navigation.addListener('didFocus', () => {
            AsyncStorage.getItem("user_id",(error, result) => {
                this.setState({
                    user_id : result
                })
            });
        })
    }

    // componentDidMount = () => {
    //     BackHandler.addEventListener('hardwareBackPress', this.handlerBack);
    // }

    // componentWillUnmount = () => {
    //     BackHandler.removeEventListener('hardwareBackPress', this.handlerBack);
    // }

    // _onBackPress = () => {
    //     BackHandler.removeEventListener('hardwareBackPress', this.handlerBack)
    // }

    // handlerBack = () => {
    //     Alert.alert("Exit","Are you sure want to exit from this screen ? your design will be lost",[
    //         {text: 'Ya', onPress: () => 
    //             {
    //                 this.props.navigation.goBack(null)
    //             }
    //         },
    //         {text: 'Tidak', style : 'cancel'},
    //     ])
    //     return true
    // }

    _colorPickerVisible = (visible) => {
        this.setState({colorPickerVisible: visible});
    }
    
    _textSettingVisible = (visible, key, color, content, fontSize, index) => {
        this.setState({
            textSettingVisible: visible,
            selectedText: key,
            textColor: color,
            textValue: content,
            textSizeValue: fontSize,
            textIndex: index
            
        });
        // ToastAndroid.show(""+this.state.textIndex, ToastAndroid.SHORT)
    }

    _setFullscreen = () => {
        this.setState({fullscreen: !this.state.fullscreen});
    }

    _bgColorChange = (color) => {
        this.setState({bgColor: color})
    }

    _addText = () => {
        let new_text = { 
            key:  this.state.textData.length == 0 ? 0 : this.state.textData[this.state.textData.length-1].key+1, 
            content: 'new text', 
            color: '#000000',
            fontSize: 14,
            index: this.state.textData.length+this.state.imageData.length,
            fontFamily: "roboto"
        };
    
        this.setState({
            textData: [...this.state.textData, new_text]
        });

        // setTimeout(() => {this._getIndex()}, 500)
    }

    _textEdit = (key) =>{
        let newTextData = [...this.state.textData]
        newTextData[key].content = this.state.textValue
        this.setState({
           textData: newTextData
        })
    }
    
    _onChangeText = (value) =>{
        this.setState({
            textValue: value
        })
    }

    _textSizeEdit = (key) =>{
        if(this.state.textSizeValue <= 6){
            let newTextData = [...this.state.textData]
            newTextData[key].fontSize = 6
            this.setState({
                textData: newTextData
            })
        }else if(this.state.textSizeValue >= 85){
            let newTextData = [...this.state.textData]
            newTextData[key].fontSize = 85
            this.setState({
               textData: newTextData
            })
        }else{
            let newTextData = [...this.state.textData]
            newTextData[key].fontSize = this.state.textSizeValue
            this.setState({
               textData: newTextData
            })
        }
        
    }

    _onChangeFontSize = (value) =>{
        this.setState({
            textSizeValue: Number(value)
        })
    }

    _textColorChange = (color, key) =>{
        let newTextData = [...this.state.textData]
        newTextData[key].color = color
        this.setState({
           textData: newTextData,
           textColor: color
        })
    }

    _imagePicker = (pick) =>{
        if(pick == "gallery"){
            this.setState({
                imagePickModal: false
            })
            ImagePicker.openPicker({
                width: 900,
                height: 900,
                freeStyleCropEnabled: true,
                cropping: true
            }).then(image => {
                console.log(image)
                setTimeout(() => {this._addImage(image.path, image.width, image.height)}, 500)
            });
        }
        else{
            this.setState({
                imagePickModal: false
            })
            ImagePicker.openCamera({
                width: 900,
                height: 900,
                cropping: true,
                freeStyleCropEnabled: true,
            }).then(image => {
                setTimeout(() => {this._addImage(image.path, image.width, image.height)}, 500)
            });
        }
    }
      
    _imagePickModal = (visible) =>{
        this.setState({
            imagePickModal: visible
        })
    }

    _addImage = (source, width, height) =>{
        let new_image = { 
            key:  this.state.imageData.length == 0 ? 0 : this.state.imageData[this.state.imageData.length-1].key+1, 
            source: source, 
            width: Math.round(width / 3),
            height: Math.round(height / 3),
            ratio: width / height,
            index: this.state.textData.length+this.state.imageData.length
        };
    
        this.setState({
            imageData: [...this.state.imageData, new_image]
        });
        console.log("sadsad", this.state.imageData)
    }

    _imageSettingVisible = (visible, key, source, width, height, ratio, index) => {
        this.setState({
            imageSettingVisible: visible,
            selectedImage: key,
            selectedSource: source,
            selectedWidth: width,
            selectedHeight: height,
            ratio: ratio,
            imageIndex: index
        });
        // ToastAndroid.show(""+this.state.imageIndex, ToastAndroid.SHORT)
    }

    _onChangeImageWidth = (value) =>{
        this.setState({
            selectedWidth: Number(value)
        })
    }

    _onChangeImageHeight = (value) =>{
        this.setState({
            selectedHeight: Number(value)
        })
    }

    _imageWidthEdit = (key) =>{
        if(this.state.selectedWidth <= 75){
            let newImageData = [...this.state.imageData]
            newImageData[key].width = 75
            newImageData[key].height = Math.round(75 / this.state.ratio)
            this.setState({
                selectedWidth: 75,
                selectedHeight: Math.round(75 / this.state.ratio),
                imageData: newImageData
            })
        }else if(this.state.selectedWidth >= 900){
            let newImageData = [...this.state.imageData]
            newImageData[key].width = 900
            newImageData[key].height = Math.round(900 / this.state.ratio)
            this.setState({
                selectedWidth: 900,
                selectedHeight: Math.round(900 / this.state.ratio),
                imageData: newImageData
            })
        }else{
            let newImageData = [...this.state.imageData]
            newImageData[key].width = this.state.selectedWidth
            newImageData[key].height = Math.round(this.state.selectedWidth / this.state.ratio)
            this.setState({
                selectedWidth: this.state.selectedWidth,
                selectedHeight: Math.round(this.state.selectedWidth / this.state.ratio),
                imageData: newImageData
            })
        }
    }

    _imageHeightEdit = (key) =>{
        if(this.state.selectedHeight <= 75){
            let newImageData = [...this.state.imageData]
            newImageData[key].height = 75
            newImageData[key].width = Math.round(75 * this.state.ratio)
            this.setState({
                selectedHeight: 75,
                selectedWidth: Math.round(75 * this.state.ratio),
                imageData: newImageData
            })
        }else if(this.state.selectedHeight >= 900){
            let newImageData = [...this.state.imageData]
            newImageData[key].height = 900
            newImageData[key].width = Math.round(900 * this.state.ratio)
            this.setState({
                selectedHeight: 900,
                selectedWidth: Math.round(900 * this.state.ratio),
                imageData: newImageData
            })
        }else{
            let newImageData = [...this.state.imageData]
            newImageData[key].height = this.state.selectedHeight
            newImageData[key].width = Math.round(this.state.selectedHeight * this.state.ratio)
            this.setState({
                selectedHeight: this.state.selectedHeight,
                selectedWidth: Math.round(this.state.selectedHeight * this.state.ratio),
                imageData: newImageData
            })
        }
    }
    
    _layerForward = (type, key) =>{
        if(type == "image"){
            var index = this.state.textData.findIndex(p => p.index == this.state.imageIndex+1)
            if(index == -1){
                
                index = this.state.imageData.findIndex(p => p.index == this.state.imageIndex+1)

                if(index != -1){
                    let newImageData = [...this.state.imageData]

                    newImageData[index].index = this.state.imageIndex
                    newImageData[key].index = this.state.imageIndex+1
                    this.setState({
                        imageData: newImageData,
                        imageIndex: this.state.imageIndex+1
                    })
                }
                
            }else{
                let newTextData = [...this.state.textData]
                newTextData[index].index = this.state.imageIndex

                let newImageData = [...this.state.imageData]
                newImageData[key].index = this.state.imageIndex+1
                this.setState({
                    textData: newTextData,
                    imageData: newImageData,
                    imageIndex: this.state.imageIndex+1
                })
            }
        }
        else if(type == "text"){
            var index = this.state.textData.findIndex(p => p.index == this.state.textIndex+1)
            if(index == -1){
                index = this.state.imageData.findIndex(p => p.index == this.state.textIndex+1)

                if(index != -1){
                    let newImageData = [...this.state.imageData]
                    newImageData[index].index = this.state.textIndex

                    let newTextData = [...this.state.textData]
                    newTextData[key].index = this.state.textIndex+1
                    this.setState({
                        imageData: newImageData,
                        textData: newTextData,
                        textIndex: this.state.textIndex+1
                    })
                }
                
            }else{
                let newTextData = [...this.state.textData]

                newTextData[index].index = this.state.textIndex
                newTextData[key].index = this.state.textIndex+1
                this.setState({
                    textData: newTextData,
                    textIndex: this.state.textIndex+1
                })
            }
        }
    }

    _layerBack = (type, key) => {
        if(type == "image"){
            var index = this.state.textData.findIndex(p => p.index == this.state.imageIndex-1)
            if(index == -1){
                
                index = this.state.imageData.findIndex(p => p.index == this.state.imageIndex-1)

                if(index != -1){
                    let newImageData = [...this.state.imageData]

                    newImageData[index].index = this.state.imageIndex
                    newImageData[key].index = this.state.imageIndex-1
                    this.setState({
                        imageData: newImageData,
                        imageIndex: this.state.imageIndex-1
                    })
                }
                
            }else{
                let newTextData = [...this.state.textData]
                newTextData[index].index = this.state.imageIndex

                let newImageData = [...this.state.imageData]
                newImageData[key].index = this.state.imageIndex-1
                this.setState({
                    textData: newTextData,
                    imageData: newImageData,
                    imageIndex: this.state.imageIndex-1
                })
            }
        }
        else if(type == "text"){
            var index = this.state.textData.findIndex(p => p.index == this.state.textIndex-1)
            if(index == -1){
                index = this.state.imageData.findIndex(p => p.index == this.state.textIndex-1)

                if(index != -1){
                    let newImageData = [...this.state.imageData]
                    newImageData[index].index = this.state.textIndex

                    let newTextData = [...this.state.textData]
                    newTextData[key].index = this.state.textIndex-1
                    this.setState({
                        imageData: newImageData,
                        textData: newTextData,
                        textIndex: this.state.textIndex-1
                    })
                }
                
            }else{
                let newTextData = [...this.state.textData]

                newTextData[index].index = this.state.textIndex
                newTextData[key].index = this.state.textIndex-1
                this.setState({
                    textData: newTextData,
                    textIndex: this.state.textIndex-1
                })
            }
        }
    }

    _layerToFront = (type, key) => {
        if(type == "image"){
            let newTextData = [...this.state.textData]
            let newImageData = [...this.state.imageData]
            let length = this.state.textData.length + this.state.imageData.length
    
            for (let i = this.state.imageIndex+1; i < length; i++){
                let index = this.state.textData.findIndex(p => p.index == i)
                if(index != -1){
                    newTextData[index].index = newTextData[index].index-1
                }
            }
    
            for (let i = this.state.imageIndex+1; i < length; i++){
                let index = this.state.imageData.findIndex(p => p.index == i)
                if(index != -1){
                    newImageData[index].index = newImageData[index].index-1
                }
            }
    
            newImageData[key].index = length-1
            this.setState({
                textData: newTextData,
                imageData: newImageData,
                imageIndex: length-1
            })    
        
        }else if(type == "text"){
            let newTextData = [...this.state.textData]
            let newImageData = [...this.state.imageData]
            let length = this.state.textData.length + this.state.imageData.length

            for (let i = this.state.textIndex+1; i < length; i++){
                let index = this.state.textData.findIndex(p => p.index == i)
                if(index != -1){
                    newTextData[index].index = newTextData[index].index-1
                }
            }

            for (let i = this.state.textIndex+1; i < length; i++){
                let index = this.state.imageData.findIndex(p => p.index == i)
                if(index != -1){
                    newImageData[index].index = newImageData[index].index-1
                }
            }

            newTextData[key].index = length-1
            this.setState({
                textData: newTextData,
                imageData: newImageData,
                textIndex: length-1
            })

        }
        
    }

    _layerToBack = (type, key) => {
        if(type == "image"){
            let newTextData = [...this.state.textData]
            let newImageData = [...this.state.imageData]
    
            for (let i = this.state.imageIndex-1; i >= 0; i--){
                let index = this.state.textData.findIndex(p => p.index == i)
                if(index != -1){
                    newTextData[index].index = newTextData[index].index+1
                }
            }
    
            for (let i =this.state.imageIndex-1; i >= 0; i--){
                let index = this.state.imageData.findIndex(p => p.index == i)
                if(index != -1){
                    newImageData[index].index = newImageData[index].index+1
                }
            }
    
            newImageData[key].index = 0
            this.setState({
                textData: newTextData,
                imageData: newImageData,
                imageIndex: 0
            })    
        
        }else if(type == "text"){
            let newTextData = [...this.state.textData]
            let newImageData = [...this.state.imageData]

            for (let i =this.state.textIndex-1; i >= 0; i--){
                let index = this.state.textData.findIndex(p => p.index == i)
                if(index != -1){
                    newTextData[index].index = newTextData[index].index+1
                }
            }

            for (let i =this.state.textIndex-1; i >= 0; i--){
                let index = this.state.imageData.findIndex(p => p.index == i)
                if(index != -1){
                    newImageData[index].index = newImageData[index].index+1
                }
            }

            newTextData[key].index = 0
            this.setState({
                textData: newTextData,
                imageData: newImageData,
                textIndex: 0
            })

        }
    }

    _indexUpdate = (type) =>{
        let newTextData = [...this.state.textData]
        let newImageData = [...this.state.imageData]
        let length = this.state.textData.length + this.state.imageData.length

        for (let i = type+1; i < length; i++){
            let index = this.state.textData.findIndex(p => p.index == i)
            if(index != -1){
                newTextData[index].index = newTextData[index].index-1
            }
        }

        for (let i = type+1; i < length; i++){
            let index = this.state.imageData.findIndex(p => p.index == i)
            if(index != -1){
                newImageData[index].index = newImageData[index].index-1
            }
        }

        this.setState({
            textData: newTextData,
            imageData: newImageData,
        })    

    }

    _deleteText = (key) =>{

        this._indexUpdate(this.state.textIndex)

        let text = this.state.textData.filter( (data) => data.key !== key );

        this.setState({
            textData: text,
            textSettingVisible: false
        });
    }

    _deleteImage = (key) =>{

        this._indexUpdate(this.state.imageIndex)

        let image = this.state.imageData.filter( (data) => data.key !== key );
        
        this.setState({
            imageData: image,
            imageSettingVisible: false
        });
    }

    _fontSetting = (visible) => {
        this.setState({
            fontSetVisible: visible,
            textSettingVisible: false
        });
    }

    _setSelectedFont = (font) =>{
        this.setState({
            selectedFontFamily: font
        })
    }

    _saveFont = (key) =>{
        let newTextData = [...this.state.textData]
        newTextData[key].fontFamily = this.state.selectedFontFamily
        this.setState({
            textData: newTextData,
            fontSetVisible: false,
            textSettingVisible: true
        })
    }
    
    // _getIndex = () =>{
    //     var index = this.state.textData.findIndex(p => p.index == 4)
        
    //     Alert.alert("", ""+index)
    // }
    
    _uploadHandler = async(Uri) =>{
        const uriPart = Uri.split('.');
        const fileExtension = uriPart[uriPart.length - 1];

        const data = new FormData()

        data.append("user_id", this.state.user_id);
        data.append("name_design", this.state.nameValue);
        data.append("image", 
            {
                uri: Uri,
                name: `photo.${fileExtension}`,
                type: `image/${fileExtension}`
            }
        )

        await uploadDesign( data ).then(() => {
            if(responseDesign.status){
                ToastAndroid.show("Upload Success", ToastAndroid.SHORT)
            }
            else{
                ToastAndroid.show("Upload Failed", ToastAndroid.SHORT)
            }
        })
    }

    _saveImage = () =>{
        this.requestExternalStoragePermission().then(res =>{
            if(res === PermissionsAndroid.RESULTS.GRANTED){
                captureRef(this.child.refs.viewShot, {
                    format:"jpg",
                    width: PixelRatio.getPixelSizeForLayoutSize(Dimensions.get('window').width),
                    height: PixelRatio.getPixelSizeForLayoutSize(Dimensions.get('window').height)
                }).then(uri =>{
                    this._uploadHandler(uri)
                        CameraRoll.saveToCameraRoll(uri,'photo').then(r =>
                            {
                                this.setState({nameVisible: false});
                                Alert.alert("","Saved to storage/DCIM")
                            })
                    },
                    error => console.error("Oops, Something Went Wrong", error)
                )
            }else{
                Alert.alert("No Permission")
            }
        })
    }

    requestExternalStoragePermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                title: 'My App Storage Permission',
                message: 'My App needs access to your storage ' +
                    'so you can save your photos',
                buttonNeutral: 'Ask Me Later',
                buttonNegative: 'Cancel',
                buttonPositive: 'OK',
                },
            );
            return granted;
        }catch (err) {
            console.error('Failed to request permission ', err);
            return null;
        }
      };

    _nameVisible = (visible) => {
        this.setState({nameVisible: visible});
    }

    _onChangeName = (value) =>{
        this.setState({
            nameValue: value
        })
    }

    render = () => {
        return <DesignScreenView
            onRef={ref => (this.child = ref)}
            state = {this.state}
            method = {this.method}
        />
    }

}