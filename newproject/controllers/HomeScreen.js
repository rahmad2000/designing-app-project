import React from 'react';
import { ToastAndroid, Alert } from 'react-native';
import HomeScreenView from "../views/HomeScreenView";
import { getProduct, responseProduct } from "../models/Home";

export default class HomeScreen extends React.Component {
    
    constructor(props) {

        super(props);

        this.state = {
            modalVisible : false,
            refreshing : false,
            search: ""
        }

        this.method = {
            openDrawer: this.openDrawer.bind(this),
            onRefresh: this.onRefresh.bind(this),
            onChangeSearch: this._onChangeSearch.bind(this),
            onSubmitSearch: this._onSubmitSearch.bind(this)
        }

        this.content = [];

    }

    onRefresh = () => {
        this.setState({refreshing: true});
        this.productHandler().then(() => {
          this.setState({refreshing: false, search: ""});
        }).catch(( error ) => {
            this.setState({refreshing: false});
        });
    }

    onLoading = () => {
        this.setState({modalVisible: true});
        this.productHandler().then(() => {
          this.setState({modalVisible: false});
        }).catch(( error ) => {
            this.setState({modalVisible: false});
        });
    }

    componentWillMount = () => {
        this.onLoading();
    }

    openDrawer = () => {
        this.props.navigation.openDrawer();
    }

    productHandler = async () => {
        await getProduct()
        .then(() => {
            this.setState({
                dataComplete: responseProduct.data,
                data: responseProduct.data,
            });
        });
    }

    _onChangeSearch = (value) =>{
        this.setState({
            search: value
        })
    }

    _onSubmitSearch = (search) =>{
        let dataSearch = this.state.dataComplete.filter((str) => {return str.name_product.toLowerCase().indexOf(search.toLowerCase()) !== -1})
        // let dataSearch = this.state.dataComplete.filter( (data) => data.name_product == search );
        if(search == ""){
            this.productHandler()
        }else if(dataSearch.length < 1){
            ToastAndroid.show("Data not found for "+search, ToastAndroid.SHORT)
        }else{
             this.setState({
                data: dataSearch
            })
        }
       
    }

    
    onDetailPress = async (params) => {
        this.props.navigation.push("Detail",params)
    }

    render = () => {
        return <HomeScreenView 
        state={this.state} 
        method={this.method}
        onDetailPress = {(params) => this.onDetailPress(params)}
        />
    }

}