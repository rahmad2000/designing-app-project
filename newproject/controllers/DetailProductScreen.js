import React, { Component } from 'react';
import { AsyncStorage, BackHandler, Alert, ToastAndroid } from 'react-native';
import { addToCart, responseAddCart } from '../models/Cart';
import DetailProductScreenView from '../views/DetailProductScreenView';

export default class DetailProductScreen extends React.Component {

    constructor(props) {
        super(props);
        
        const { navigation } = this.props;
        this.state = {
            product_id : navigation.getParam('product_id', ''),
            name_product : navigation.getParam('name_product', ''),
            image : navigation.getParam('image', ''),
            category : navigation.getParam('category', ''),
            price : navigation.getParam('price', ''),
            detail : navigation.getParam('detail', ''),
            buyCount: 1,
            modalVisible: false
        }

        this.method={
            onAddToCart: this.cartHandler.bind(this),
            colorOption : this._colorOption.bind(this),
            sizeOption : this._sizeOption.bind(this),
            productCount : this._productCount.bind(this),
            setImageFull: this.setImageFull.bind(this),
            setModalVisible: this.setModalVisible.bind(this)
        }
        
    }

    componentWillMount = () => {
        AsyncStorage.getItem("user_id",(error, result) => {
            this.setState({
                user_id : result
            })
        });
    }

    componentWillUnmount = () => {
        BackHandler.addEventListener( 'hardwareBackPress', this.handlerBack);
    }

    componentDidMount = () => {
        BackHandler.addEventListener( 'hardwareBackPress', this.handlerBack);
    }

    handlerBack = () => {   
        this.props.navigation.goBack(null)
        return true;
    }

    backPress = () => {
        this.props.navigation.goBack(null)
    }

    setModalVisible(visible){
        this.setState({modalVisible: visible})
    }

    setImageFull(showImage){
        this.setState({fullscreen: showImage})
    }

    _colorOption = (option) =>{
        if(option == this.state.selectedColor){
            this.setState({
                selectedColor: null
            })
        }else{
            this.setState({
                selectedColor: option
            })
        }
       
    }
    _sizeOption = (option) =>{
        if(option == this.state.selectedSize){
            this.setState({
                selectedSize: null
            })
        }else{
            this.setState({
                selectedSize: option
            })
        }  
    }

    _productCount = (value) =>{
        if(value < 1){
            this.setState({
                buyCount: 1,
                totalPrice: this.state.price
            })
        }else if(value > 100){
            this.setState({
                buyCount: 100,
                totalPrice: this.state.price * 100
            })
            ToastAndroid.show("Maximum order is 100 !", ToastAndroid.SHORT)
        }else{
            this.setState({
                buyCount: value,
                totalPrice: this.state.price * value
            })
        }
    }

    cartHandler = async() => {
        // Alert.alert("", ""+this.state.totalPrice)
        if(this.state.selectedColor == null || this.state.selectedSize == null || this.state.buyCount == "" ){
            ToastAndroid.show("Please input all fields", ToastAndroid.SHORT)
        }else{
            await addToCart( this.state.user_id, this.state.product_id, this.state.buyCount, this.state.totalPrice, this.state.selectedColor, this.state.selectedSize).then(()=>{
                if(responseAddCart.status){
                    this.setState({
                        modalVisible: false
                    })
                    ToastAndroid.show("Success", ToastAndroid.SHORT)
                }
                else{
                    ToastAndroid.show("Failed", ToastAndroid.SHORT)
                }
            }) 
        }
        
    }

    render(){
        return <DetailProductScreenView
            method = {this.method}
            state = {this.state}
        />
    }
}