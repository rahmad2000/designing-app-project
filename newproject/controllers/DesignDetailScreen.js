import React from 'react';
import { AsyncStorage, Alert, ToastAndroid } from 'react-native';
import DesignDetailScreenView from "../views/DesignDetailScreenView";
import { uploadProduct, responseProduct } from "../models/Upload";

export default class DesignDetailScreen extends React.Component {
    
    constructor(props) {

        super(props);

        this.state = {
            designId: this.props.navigation.getParam("designId", ""),
            designImage: this.props.navigation.getParam("designImage", ""),
            designName: this.props.navigation.getParam("designName", ""),
            designer: this.props.navigation.getParam("designUser", ""),
            modalVisible: false,
            category: "T-Shirt",
            sizeS: false,
            sizeM: false,
            sizeL: false,
            sizeXL: false,
            sizeOption1: "",
            sizeOption2: "",
            sizeOption3: "",
            sizeOption4: ""
        }

        this.method = {
            uploadModal: this._uploadModal.bind(this),
            addProduct: this._addProduct.bind(this),
            onCategoryChange: this._onCategoryChange.bind(this),
            productName: this._productName.bind(this),
            productPrice: this._productPrice.bind(this),
            productDescription: this._productDescription.bind(this),
            sizeOption: this._sizeOption.bind(this)
        }

        this.content = [];

    }

    componentWillMount = () => {
        AsyncStorage.getItem("user_id", (error, result) => {
            this.setState({
                user_id : result
            })
        })
    }


    _uploadModal = (visible) =>{
        this.setState({
            modalVisible: visible
        })
    }

    _onCategoryChange = (value) =>{
        this.setState({
            category: value
        })
    }

    _productName = (value) =>{
        this.setState({
            name_product: value
        })
    }

    _productPrice = (value) =>{
        this.setState({
            price: value
        })
    }

    _productDescription = (value) =>{
        this.setState({
            description: value
        })
    }

    _sizeOption = (option) =>{
        if( option == 1){
            this.setState({
                sizeS: !this.state.sizeS
            })
            if(this.state.sizeS){
                this.setState({
                    sizeOption1: ""
                })
            }else{
                this.setState({
                    sizeOption1: "S/"
                })
            }
        }else if( option == 2 ){
            this.setState({
                sizeM: !this.state.sizeM
            })
            if(this.state.sizeM){
                this.setState({
                    sizeOption2: ""
                })
            }else{
                this.setState({
                    sizeOption2: "M/"
                })
            }
        }else if( option == 3 ){
            this.setState({
                sizeL: !this.state.sizeL
            })
            if(this.state.sizeL){
                this.setState({
                    sizeOption3: ""
                })
            }else{
                this.setState({
                    sizeOption3: "L/"
                })
            }
        }else if( option == 4 ){
            this.setState({
                sizeXL: !this.state.sizeXL
            })
            if(this.state.sizeXL){
                this.setState({
                    sizeOption4: ""
                })
            }else{
                this.setState({
                    sizeOption4: "XL"
                })
            }
        }
    }
    
    _addProduct = async(image) =>{
        // Alert.alert("", this.state.sizeOption1+this.state.sizeOption2+this.state.sizeOption3+this.state.sizeOption4)
        let size = this.state.sizeOption1+this.state.sizeOption2+this.state.sizeOption3+this.state.sizeOption4
        await uploadProduct( this.state.user_id, this.state.name_product, image, this.state.category, this.state.price, this.state.description, size, this.state.designId ).then(() => {
            if(responseProduct.status){
                ToastAndroid.show("Success", ToastAndroid.SHORT),
                this.setState({
                    modalVisible: false
                })
                this.props.navigation.push("AdminDesignList")
            }
            else{
                ToastAndroid.show("Failed", ToastAndroid.SHORT)
            }
        })
    }
    

    render = () => {
        return <DesignDetailScreenView state={this.state} method={this.method} />
    }

}