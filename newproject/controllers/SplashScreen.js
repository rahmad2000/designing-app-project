import React, { Component, Alert } from 'react';
import { AsyncStorage} from 'react-native';
import SplashScreenView from "../views/SplashScreenView";


export default class SplashScreen extends React.Component {
    
    constructor(props) {

        super(props);

        this.state = {}

        this.content = [];

    }

    componentWillMount = () => {
        this._subscribe = this.props.navigation.addListener('didFocus', () => {
            AsyncStorage.getItem("level",(error, result) => {
                if(result == "admin"){
                    setTimeout(() => {
                        this.props.navigation.replace("Admin");            
                    }, 2000);
                }else if(result == "user"){
                    setTimeout(() => {
                        this.props.navigation.replace("Drawer");            
                    }, 2000);
                }else{
                    setTimeout(() => {
                        this.props.navigation.replace("Introduction");            
                    }, 2000);
                }
            });
        });
    }

    render = () => {
        return <SplashScreenView />
    }

}