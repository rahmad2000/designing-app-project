import React from 'react';
import { AsyncStorage, Alert, ToastAndroid } from 'react-native';
import AdminScreenView from "../views/AdminScreenView";

export default class AdminScreen extends React.Component {
    
    constructor(props) {

        super(props);

        this.state = {
        }

        this.method = {
            logout: this.logout.bind(this),
            designListPress: this._designListPress.bind(this),
            orderListPress: this._orderListPress.bind(this),
            productListPress: this._productListPress.bind(this)
        }

        this.content = [];

    }

    _designListPress = () =>{
        this.props.navigation.push("AdminDesignList")
    }

    _orderListPress = () =>{
        this.props.navigation.push("DesignList")
    }

    _productListPress = () =>{
        this.props.navigation.push("AdminProduct")
    }

    logout = () => {
        Alert.alert("Logout", "Anda yakin ingin logout?", [
            {text: 'Ya', onPress: () => {
                AsyncStorage.clear(() => {
                    this.props.navigation.push("Login");
                })
            }},
            {text: 'Tidak', onPress: () => {
                this.props.navigation.push("Admin");
            }},
        ],{cancelable: false})
    }

    render = () => {
        return <AdminScreenView state={this.state} method={this.method} />
    }

}