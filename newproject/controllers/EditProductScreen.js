import React from 'react';
import { AsyncStorage, Alert, ToastAndroid } from 'react-native';
import EditProductScreenView from "../views/EditProductScreenView";
import { responseUpdate, updateProduct } from "../models/Update";

export default class EditProductScreen extends React.Component {
    
    constructor(props) {

        super(props);

        this.state = {
            productId: this.props.navigation.getParam("productId", ""),
            productImage: this.props.navigation.getParam("productImage", ""),
            productName: this.props.navigation.getParam("productName", ""),
            productCategory: this.props.navigation.getParam("productCategory", ""),
            productPrice: this.props.navigation.getParam("productPrice", ""),
            productDetail: this.props.navigation.getParam("productDetail", "")
        }

        this.method = {
            onCategoryChange: this._onCategoryChange.bind(this),
            productName: this._productName.bind(this),
            productPrice: this._productPrice.bind(this),
            productDetail: this._productDetail.bind(this),
            editProduct: this._editProduct.bind(this)
        }

        this.content = [];

    }

    _onCategoryChange = (value) =>{
        this.setState({
            productCategory: value
        })
    }

    _productName = (value) =>{
        this.setState({
            productName: value
        })
    }

    _productPrice = (value) =>{
        this.setState({
            productPrice: value
        })
    }

    _productDetail = (value) =>{
        this.setState({
            productDetail: value
        })
    }

    _editProduct = async() =>{
        await updateProduct( this.state.productId, this.state.productName, this.state.productCategory, this.state.productPrice, this.state.productDetail ).then(() => {
            if(responseUpdate.status){
                ToastAndroid.show("Success", ToastAndroid.SHORT)
                this.props.navigation.push("AdminProduct")
            }
            else{
                ToastAndroid.show("Failed", ToastAndroid.SHORT)
            }
        })
    }

    render = () => {
        return <EditProductScreenView state={this.state} method={this.method} />
    }

}