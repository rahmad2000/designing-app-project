import React from 'react';
import { ToastAndroid } from 'react-native';
import ItemCategoryScreenView from "../views/ItemCategoryView";

import { getFilter, responseFilter } from "../models/Category";

export default class ItemCategoryScreen extends React.Component {
    
    constructor(props) {

        super(props);

        this.state = {
            category : this.props.navigation.getParam("category", ""),
            modalVisible: false,
            refreshing : false
        }

        this.method = {
            onBackPress: this.onBackPress.bind(this),
            onRefresh: this.onRefresh.bind(this),
        }

    }

    onRefresh = () => {
        this.setState({refreshing: true});
        this.filterHandler().then(() => {
          this.setState({refreshing: false});
        }).catch(( error ) => {
            this.setState({refreshing: false});
        });
    }

    onLoading = () => {
        this.setState({modalVisible: true});
        this.filterHandler().then(() => {
          this.setState({modalVisible: false});
        }).catch(( error ) => {
            this.setState({modalVisible: false});
        });
    }

    componentWillMount = () => {
        this.filterHandler();
        this.onLoading();
    }

    onBackPress = () => {
        this.props.navigation.goBack(null);
    }

    filterHandler = async () => {
        await getFilter( this.state.category )
        .then(() => {
            if( responseFilter.status ){
                this.setState({
                    data: responseFilter.data
                });
            }else{
                ToastAndroid.show(""+this.state.category+" Not Available", ToastAndroid.SHORT )
            }
        });
    }

    onDetailPress = async (params) => {
        this.props.navigation.push("Detail",params)
    }

    render = () => {
        return <ItemCategoryScreenView state={this.state} method={this.method} onDetailPress = {(params) => this.onDetailPress(params)} />
    }

}