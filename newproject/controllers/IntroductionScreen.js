import React, { Component } from 'react';
import { AsyncStorage, BackHandler, ToastAndroid, Alert } from 'react-native';


import IntroductionView from '../views/IntroductionView';

export default class IntroductionScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: true,
            data : [
                {
                    id: 1,
                    title : '1.WELCOME',
                    description: 'Welcome to scratch an app where you can design your own merch',
                    thumbnail : require('../assets/images/Tshirts.jpg'),
                    
                },
                {
                    id: 2,
                    title : '2.EASY TO USE',
                    description: 'We provide you with the desinging tools inside the app its simple and very easy to use',
                    thumbnail : require('../assets/images/mugs.jpg'),

                },
                {
                    id: 3,
                    title : '3.EASY TO SELL',
                    description: 'After you upload your design on our store we will sell it for you we will make the merch and shipped it',
                    thumbnail : require('../assets/images/Caps.jpg'),
                
                },
                {
                    id: 4,
                    title : '4.LOREM IPSUM',
                    description: 'Lorem Ipsum',
                    thumbnail : require('../assets/images/Hoodies.jpg'),
                
                },

            ]
        }

        this.method = {
            onNextPress : this.onNextPress.bind(this),
            setModalVisible : this.setModalVisible.bind(this)
        }
    }


    componentWillMount = () => {
    }

    componentWillUnmount = () => {
        BackHandler.addEventListener( 'hardwareBackPress', this.handlerBack);
    }

    componentDidMount = () => {
        BackHandler.addEventListener( 'hardwareBackPress', this.handlerBack);
    }

    handlerBack = () => {
        BackHandler.exitApp();
        return true;
    }

    setModalVisible(visible){
        this.setState({modalVisible: visible})
    }

    onNextPress = () => {
        this.setState({
            modalVisible: false
        })
        this.props.navigation.push( 'Login' );
    }

    render(){
        return <IntroductionView
        state={this.state}
        method={this.method}
        onNextPress={ () => this.onNextPress() }
        />
    }
}