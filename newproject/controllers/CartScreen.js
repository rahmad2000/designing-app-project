import React from 'react';
import { ToastAndroid, Alert, AsyncStorage } from 'react-native';
import CartScreenView from "../views/CartScreenView";
import { getCart, responseCart, deleteCart, responseDelete } from "../models/Cart";

export default class CartScreen extends React.Component {
    
    constructor(props) {

        super(props);

        this.state = {
            modalVisible : false,
            refreshing : false,
        }

        this.method = {
            openDrawer: this.openDrawer.bind(this),
            onRefresh: this.onRefresh.bind(this),
            onDelete: this.onDelete.bind(this),
            checkoutPress: this._checkoutPress.bind(this)
        }

        this.content = [];

    }

    onRefresh = () => {
        this.setState({refreshing: true});
        this.cartHandler(this.state.user_id).then(() => {
          this.setState({refreshing: false});
        }).catch(( error ) => {
            this.setState({refreshing: false});
        });
    }

    onLoading = (userId) => {
        this.setState({modalVisible: true});
        this.cartHandler().then(() => {
          this.setState({modalVisible: false});
        }).catch(( error ) => {
            this.setState({modalVisible: false});
        });
    }

    componentWillMount = () => {
        this._subscribe = this.props.navigation.addListener('didFocus', () => {
            AsyncStorage.getItem("user_id",(error, result) => {
                this.setState({
                    user_id : result
                })
                this.onLoading(result);
            });
        })
    }

    openDrawer = () => {
        this.props.navigation.openDrawer();
    }

    cartHandler = async () => {
        await getCart(this.state.user_id)
        .then(() => {
            if(responseCart){
                this.setState({
                    data: responseCart.data,
                });
            }else{
                ToastAndroid.show("get data failed", ToastAndroid.SHORT);
            }
        });
    }

    deleteHandler = async (params) => {
        await deleteCart(params)
        .then(() => {
            if(responseDelete){
                ToastAndroid.show("Item deleted", ToastAndroid.SHORT);
                this.cartHandler();
            }else{
                ToastAndroid.show("Delete Failed", ToastAndroid.SHORT);
            }
        })
    }

    onDelete = async (params) => {
        Alert.alert("Delete Item", "Are you sure to delete this item?", [{ 
            text: "Yes", onPress: () => {
                this.deleteHandler(params);
            }
        },{
            text: "No", style: "cancel",
        }]);
    }
    
    _checkoutPress = () =>{
        this.props.navigation.push("Checkout", {userId: this.state.user_id})
    }

    render = () => {
        return <CartScreenView state={this.state} method={this.method} />
    }

}