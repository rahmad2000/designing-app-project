import React from 'react'
import { Image } from 'react-native'
import { createDrawerNavigator, createStackNavigator, createAppContainer } from 'react-navigation'

//APP SCREEN
import SplashScreen from '../controllers/SplashScreen'
import LoginScreen from '../controllers/LoginScreen'
import HomeScreen from '../controllers/HomeScreen'
import DesignScreen from '../controllers/DesignScreen'
import LogoutScreen from '../controllers/LogoutScreen'
import CategoryScreen from '../controllers/CategoryScreen'
import ItemCategoryScreen from '../controllers/ItemCategoryScreen'
import AdminScreen from '../controllers/AdminScreen'
import DesignDetailScreen from '../controllers/DesignDetailScreen'
import CheckoutScreen from '../controllers/CheckoutScreen'
import CartScreen from '../controllers/CartScreen'
import DetailProduct from '../controllers/DetailProductScreen'
import DesignListScreen from '../controllers/DesignListScreen'
import AdminProductScreen from '../controllers/AdminProductScreen';
import EditProductScreen from '../controllers/EditProductScreen';
import IntroductionScreen from '../controllers/IntroductionScreen'

export var Link = "http://192.168.0.9/scratch/v1/";

export const MainDrawer = (auth) => {
    // this.stats();
    return createAppContainer(
        createStackNavigator({

            Splash: {
                screen: SplashScreen
            },
            Introduction:{
                screen: IntroductionScreen
            },
            Login: {
                screen: LoginScreen
            },
            Drawer: {
                screen: MyDrawerNavigator
            },
            ItemCategory: {
                screen: ItemCategoryScreen
            },
            Detail:{
                screen: DetailProduct
            },
            Checkout: {
                screen: CheckoutScreen
            }, 
            Admin: {
                screen: AdminScreen
            },  
            AdminDesignList:{
                screen: DesignListScreen
            },
            AdminDesignDetail: {
                screen: DesignDetailScreen
            },
            AdminProduct: {
                screen: AdminProductScreen
            },
            EditProduct: {
                screen: EditProductScreen
            }

        },{        
            headerMode: 'none',
            disableOpenGesture: true,
            initialRouteName: "Splash"
        })
    )
}

const MyDrawerNavigator = createDrawerNavigator({
    Home: {
        screen: HomeScreen,
        navigationOptions: {
            drawerLabel: 'Home',
            drawerIcon: ({ tintColor }) => (
                <Image source={require('../assets/images/home.png')} style={{ width: 24, height: 24 }} />
            )
        }
    },
    Cart: {
        screen: CartScreen,
        navigationOptions: {
            drawerLabel: 'Cart',
            drawerIcon: ({ tintColor }) => (
                <Image source={require('../assets/images/cart.png')} style={{ width: 24, height: 24 }} />
            )
        }
    },
    Category: {
        screen: CategoryScreen,
        navigationOptions: {
            drawerLabel: 'Category',
            drawerIcon: ({ tintColor }) => (
                <Image source={require('../assets/images/category.png')} style={{ width: 24, height: 24 }} />
            )
        }
    },
    Design: {
        screen: DesignScreen,
        navigationOptions: {
            drawerLabel: 'Design',
            drawerIcon: ({ tintColor }) => (
                <Image source={require('../assets/images/design.png')} style={{ width: 24, height: 24 }} />
            )
        }
    },
    Logout: {
        screen: LogoutScreen,
        navigationOptions: {
            drawerLabel: 'Logout',
            drawerIcon: ({ tintColor }) => (
                <Image source={require('../assets/images/logout.png')} style={{ width: 24, height: 24 }} />
            )
        }
    }
  },{
      initialRouteName: "Home",
      contentOptions: {
          activeTintColor: '#000000'
      }
  });