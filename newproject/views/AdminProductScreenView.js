import React, { Component } from 'react'
import { Text, View, Image, TextInput, FlatList, Dimensions, ScrollView, TouchableOpacity, ImageBackground, RefreshControl, ActivityIndicator } from 'react-native'
import { Link } from '../systems/Config';

export default class AdminProductScreenView extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return <View style={{ flex: 1, backgroundColor: '#F5F5F5' }}>
            <View style={{ flexDirection: 'row', height: 56, backgroundColor: '#FFFFFF', alignItems: 'center', justifyContent: 'space-between' }}>
                <View style={{ width: 32, height: 32, marginHorizontal: 8 }}></View>
                <Text style={{ color: '#0AD48B', fontSize: 22, fontFamily: 'MrEaves-Bold' }} >SCRATCH</Text>
                <TouchableOpacity onPress={this.props.method.logout}>
                    <Image source={require('../assets/images/logout.png')} style={{ width: 24, height: 24, margin: 16, tintColor: "#0AD48B" }} />
                </TouchableOpacity>
            </View>
            <ScrollView contentContainerStyle={{flexGrow: 1}} 
                stickyHeaderIndices={[0]}
                refreshControl={
                <RefreshControl 
                    onRefresh={this.props.method.onRefresh}
                    refreshing={this.props.state.refreshing}
                />
            }> 
                <View style={{marginTop: 10}}>
                    <View style={{ flexDirection: 'row', paddingHorizontal: 16, justifyContent: 'center', paddingVertical: 10, backgroundColor: "#F5F5F5" }}>
                        <TextInput 
                            value={this.props.state.search}
                            onChangeText={(value)=>this.props.method.onChangeSearch(value)}
                            onSubmitEditing={()=>this.props.method.onSubmitSearch(this.props.state.search)}
                            placeholder={'Search...'} 
                            style={{ backgroundColor: '#FFFFFF', width: Dimensions.get('window').width / 1.3, height: 40, padding: 0, paddingLeft: 16, borderTopLeftRadius: 32, borderBottomLeftRadius: 32 }} />
                        <View style={{ backgroundColor: '#FFFFFF', borderTopRightRadius: 32, borderBottomRightRadius: 32, justifyContent: "center" }}>
                            <TouchableOpacity onPress={()=>this.props.method.onSubmitSearch(this.props.state.search)} style={{ paddingHorizontal: 16, borderTopRightRadius: 32, borderBottomRightRadius: 32, justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../assets/images/search.png')} style={{ width: 24, height: 24 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                    
                {!this.props.state.modalVisible ? 
                    <FlatList 
                        data={this.props.state.data}
                        numColumns={2}
                        columnWrapperStyle={{ justifyContent: 'space-between' }}
                        renderItem={({ item }) => 
                            <TouchableOpacity onPress={()=>this.props.method.itemProductPress(
                                {
                                    productId: item.product_id,
                                    productName: item.name_product,
                                    productImage: item.image,
                                    productCategory: item.category,
                                    productPrice: item.price,
                                    productDetail: item.detail
                                }
                            )}>
                            <View style={{ marginBottom: 16, overflow: 'hidden', width: Dimensions.get('window').width / 2.3, borderRadius: 7, elevation:3.6 }}>
                                <View style={{ backgroundColor: '#FFFFFF', alignItems: 'center', justifyContent: 'center', width: '100%', height: Dimensions.get('window').height / 4 }}>
                                    <Image source={{uri: Link + item.image}} style={{ width: '100%', height: '100%', resizeMode: 'cover' }} />
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'center', padding: 8, backgroundColor: "#FFFFFF", height: 50, alignItems: "center" }}>
                                    <Text style={{ color: '#555555', textAlign: 'center' }}>{item.name_product}</Text>
                                </View>
                            </View>
                            </TouchableOpacity>
                        }
                        keyExtractor={(item, index) => item.product_id}
                        style={{ marginTop: 10, paddingHorizontal: 16 }}                     
                    />          
                    :
                    <View style={{flex: 1, justifyContent: "center", marginTop: 36}}>
                        <ActivityIndicator size='large' color={'#555555'} />
                    </View>
                }                
            </ScrollView>
        </View>
    }
}