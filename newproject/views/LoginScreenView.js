import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, StatusBar, Dimensions, Modal, ActivityIndicator, ToastAndroid } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler';
import Input from '../component/PaperTextInput';
import Button from '../component/PaperButton';
export default class LoginScreenView extends Component {

	constructor(props) {
        super(props);
        this.state = {
            emailLogin : '',
            passwordLogin : '',
            nameRegister : '',
            emailRegister:'',
            passwordRegister:'',
        
        };
    }

    render() {
        return <ScrollView  contentContainerStyle={{ flexGrow: 1 }}>
            <View style={{flex: 1, justifyContent:'center'}}>

            <StatusBar backgroundColor={'#0AD48B'}/>
                                 
            <View style={{
                width: Dimensions.get('window').width,
                height: Dimensions.get('window').height / 2.7,
                justifyContent:'center'
                }}>
                
                            <Image 
                            source={require('../assets/images/Logoloo.png')}
                            style={{
                                alignSelf:'center',
                                width: Dimensions.get('window').width,
                                height: Dimensions.get('window').height /2.7
                                }}/>
                
                                      
            </View>

            <View style={{ flex: 1, paddingHorizontal: 24, paddingBottom: 24 }}>
                
                <View style={{borderRadius: 8, elevation: 6, backgroundColor: 'white', 
                    flex: 1 , marginTop: -60}}>
                    
                    <View style={{flexDirection:'row', justifyContent:'space-evenly', marginVertical: 8}}>    
                        
                        <Text onPress={()=>this.props.method.setLogin()} style={this.props.state.content =='1' ? {color: 'black', fontSize: 24, paddingVertical: 8, borderBottomWidth: 3, borderBottomColor: '#0AD48B',fontWeight: '500'} : {fontSize: 24, paddingVertical: 8,}}>Sign In</Text>
                        <Text onPress={()=>this.props.method.setRegister()} style={this.props.state.content =='2' ? {color: 'black', fontSize: 24, paddingVertical: 8, borderBottomWidth: 3, borderBottomColor: '#0AD48B',fontWeight: '500'} : {fontSize: 24, paddingVertical: 8,}}>Sign Up</Text>
                   
                    </View>

                    { this.props.state.content == '1' ?
                    <View style={{ alignItems: "center", flex: 1, justifyContent: "space-between", padding: 12 }}>
                        
                        <View>
                            <Input 
                                style={{
                                    marginBottom : 8
                                }}
                                width={100}
                                mode = {'outlined'}
                                placeholder = {'Type here...'}
                                label = {'Email'}
                                type = {'email-address'}
                                value = {this.props.emailValueLogin}
                                onChangeText = {this.props.onChangeEmailLogin}
                                helperType = {"info"}
                                visible = {this.state.emailLogin}
                            />

                            <Input 
                                style={{
                                    marginBottom : 16
                                }}
                                mode = {'outlined'}
                                placeholder = {'Type here...'}
                                label = {'Password'}
                                secureTextEntry = {true}
                                value = {this.props.passwordValueLogin}
                                onChangeText = {this.props.onChangePasswordLogin}
                                helperType = {"info"}
                                visible = {this.state.passwordLogin}
                            />
                        </View>

                        <Button 
                            style={{
                                justifyContent: 'center',
                                height : 50,
                                width : Dimensions.get('window').width / 2
                            }}
                            dark = {true}
                            color = {'#0AD48B'}
                            mode = {'contained'}
                            label = {'Login'}
                            onPress = {this.props.method.onLogin}
                        />

                    </View>
                    :
                    
                    this.props.state.content == '2' ?
                    <View style={{ alignItems: "center", flex: 1, justifyContent: "space-between", padding: 12 }}>
                    
                        <View>
                            <Input 
                                style={{
                                    marginBottom : 8
                                }}
                                mode = {'outlined'}
                                placeholder = {'Type here...'}
                                label = {'Name'}
                                value = {this.props.nameValueRegister}
                                onChangeText = {this.props.onChangeNameRegister}
                                helperType = {"info"}
                                visible = {this.state.nameRegister}
                            />

                            <Input 
                                style={{
                                    marginBottom : 8
                                }}
                                mode = {'outlined'}
                                placeholder = {'Type here...'}
                                label = {'Email'}
                                type = {'email-address'}
                                secureTextEntry = {false}
                                value = {this.props.emailValueRegister}
                                onChangeText = {this.props.onChangeEmailRegister}
                                helperType = {"info"}
                                visible = {this.state.emailLogin}
                            />

                            <Input 
                                style={{
                                    marginBottom : 16
                                }}
                                mode = {'outlined'}
                                placeholder = {'Type here...'}
                                label = {'Password'}
                                secureTextEntry = {true}
                                value = {this.props.passwordValueRegister}
                                onChangeText = {this.props.onChangePasswordRegister}
                                helperType = {"info"}
                                visible = {this.state.passwordRegister}
                            />
                        </View>
                        
                        <Button 
                            style={{
                                justifyContent: 'center',
                                height : 50,
                                width : Dimensions.get('window').width / 2
                            }}
                            dark = {true}
                            color = {'#0AD48B'}
                            mode = {'contained'}
                            label = {'Register'}
                            onPress = {this.props.method.onRegister}
                        />

                    </View> 
                    :
                    <View></View>
                    }

                </View>
                
            </View>
            
            </View>

            <Modal
            animationType={"fade"}
            transparent={true}
            visible={this.props.state.onLoading}>
                <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                    <View style={{ backgroundColor: "#333333", padding: 24, justifyContent: "center", alignItems: "center", borderRadius: 8 }}>
                        <ActivityIndicator size= 'large' color= "white"/>
                        <Text style={{color: "#FFFFFF", marginTop: 8}}>Please Wait</Text>
                    </View>
                </View>
            </Modal>

            </ScrollView>
            
            // <View style={{flex:1}}> 
            //     <View style={{height: 135}}>
            //         <View style={{width:'100%', height:75, backgroundColor:'#3498db'}}>     
            //             <View style={{width:120, height:135, backgroundColor:'#3498db', alignSelf:'center', paddingTop:10,borderBottomEndRadius:100,borderBottomStartRadius:100, justifyContent:'center',alignContent:"center",alignItems:'center'}}>
            //                 <Image source={require('../assets/images/Logolo.png')} style={{width:70, height:70}}></Image>
            //             </View>
            //          </View>
            //     </View>          
            //     <ScrollView contentContainerStyle={{flexGrow:1}}>
            //         <View style={{marginTop:50,flex:1,alignContent:"center",alignItems:'center', paddingHorizontal:20}}>
            //             <Text style={{color:'#3498db', fontSize: 20,}}>Email</Text>
            //             <TextInput style={{borderBottomColor:'#17202A', borderBottomWidth:2, alignSelf:'stretch', textAlign:'center'}} placeholder='Please enter email'></TextInput>

            //             <Text style={{color:'#3498db', fontSize: 20, marginTop:37}}>Password</Text>
            //             <TextInput secureTextEntry={true} style={{borderBottomColor:'#17202A', borderBottomWidth:2, alignSelf:'stretch', textAlign:'center'}} placeholder='Please enter password'></TextInput>

            //             <TouchableOpacity style={{backgroundColor:'#3498db', paddingVertical:13, paddingHorizontal:83, borderRadius:50, marginTop:68}}>
            //                 <Text style={{color:'#F7F9F9', fontWeight:'bold'}}>LOG IN</Text>
            //             </TouchableOpacity>

            //             <Text style={{color:'#17202A', fontSize:13, marginTop:40, marginBottom:20}}>Don't have an account?
            //                 <Text onPress={()=> this.props.onPressRegisterOffice()} style={{color:'#3498db'}}> Sign Up</Text>
            //             </Text>
            //         </View>
            //     </ScrollView>
            // </View>
    }
}