import React, { Component } from 'react'
import { Text, View, Image, Dimensions, TouchableHighlight, Modal, TouchableOpacity, TextInput } from 'react-native'
import {Link} from '../systems/Config'
import Input from '../component/PaperTextInput';
import { Button } from 'react-native-paper';


export default class DetailProductScreenView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            infoDetail: true,
            ColorOption : "2",
        };
    }
    
    

    render() {
        return (
        <View style={{flex: 1}}>
                
            <View style={{
                height: Dimensions.get('window').height / 12,
                backgroundColor:"#0AD48B",
                elevation:16,
                paddingHorizontal: 10,
                flexDirection:"row",
                justifyContent:'space-between',
                alignItems:'center'}}>
                    
                <TouchableOpacity activeOpacity={0.5}
                    style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Image
                        source={require('../assets/images/icons8-left-50.png')}
                        style={{ width: 30, height: 30, tintColor: "#FFFFFF" }}/>
                </TouchableOpacity>

                <Text style={{color:"#FFFFFF", fontSize:14, fontFamily:"robotoregular", justifyContent:'center'}}>ITEM DETAIL</Text>
                
                {!this.props.state.fullscreen ? 

                    <TouchableOpacity activeOpacity={0.5} onPress={()=>this.props.method.setImageFull(true)}
                        style={{justifyContent: 'center', alignItems: 'center'}}>
                        <Image
                            source={require('../assets/images/full-screen.png')}
                            style={{ width: 20, height: 20, tintColor: "#FFFFFF" }}
                        />
                    </TouchableOpacity>

                    :

                    <TouchableOpacity activeOpacity={0.5} onPress={()=>this.props.method.setImageFull(false)}
                        style={{justifyContent: 'center', alignItems: 'center'}}>
                        <Image
                            source={require('../assets/images/full-screen-exit.png')}
                            style={{ width: 20, height: 20, tintColor: "#FFFFFF" }}
                        />
                    </TouchableOpacity>
                }
            </View>           
    
            <View style={{flex: 1, alignItems:'center', justifyContent: "flex-end", padding: 10}}>                    
                
                <View style={!this.props.state.fullscreen ? {justifyContent:"flex-start", position: "absolute", top: 0, width: Dimensions.get("window").width, height: "85%"} : {justifyContent:"flex-start", position: "absolute", top: 0, width: Dimensions.get("window").width, height: "100%"} }>
                    <Image 
                        resizeMode = {!this.props.state.fullscreen ? "cover" : "contain"}
                        source = {{uri : Link + this.props.state.image}} 
                        style={{
                            width: "100%",
                            height: "100%"
                        }}
                    />
                </View>

                <View style={!this.props.state.fullscreen ? {backgroundColor:'rgba(255,255,255,0.95)', elevation:6, borderRadius:16,width: "100%", height:Dimensions.get('window').height/2} : {display: "none"}}>
                
                    <View style={{padding:10,marginVertical:10}}>
                        <Text style={{fontFamily:'roboto',fontSize:20,color:"black",alignSelf:'center'}}> {this.props.state.name_product} </Text>
                        <Text style={{fontSize:18,color:'#77D353',alignSelf:'center',marginTop:10}}> Rp.{this.props.state.price} </Text> 
                        <Text style={{marginVertical:20,fontSize:14,color:'grey',alignSelf:'center'}}>{this.props.state.detail}</Text>
                    </View>

                    <Button 
                        style={{justifyContent:'center',borderRadius:50,position:'absolute',bottom:20,alignSelf:'center'}}
                        width={300}
                        height={50}
                        icon={({ size, color }) => (
                            <Image
                            source={require('../assets/images/icons8-shopping-bag-50.png')}
                            style={{ width: 20, height: 20, tintColor: "white",marginHorizontal:-10 }}
                            />
                        )}
                        mode="contained"
                        onPress={() =>{this.props.method.setModalVisible(true);}}>
                        ADD TO BAG
                    </Button> 
            
                </View>

            </View>

            <Modal
                animationType="fade"
                transparent={true}
                visible={this.props.state.modalVisible}
                onRequestClose={()=>{
                    this.props.method.setModalVisible(false);
                }}>

                <View style={{
                    flex: 1,
                    padding: 20,
                    justifyContent:'center', 
                    backgroundColor: 'rgba(0,0,0,0.7)'}}>
                            
                        <View style={{
                            padding: 10,
                            backgroundColor: "#F5F5F5",
                            borderRadius: 15}}>
                    
                            <Text style={{alignSelf:'center', fontSize: 24, fontFamily:'MrEaves-Bold', color:'black'}}>Order Detail</Text>

                            <View style={{marginTop: 10}}>
                                <Text style={{fontSize:12, color: "#333333"}}>Color Option</Text>
                                <View style={{flexDirection:"row",justifyContent:"flex-start"}}>
                                    <TouchableOpacity  onPress={()=>this.props.method.colorOption('#0C0A3E')} style={this.props.state.selectedColor == '#0C0A3E' ? {elevation:8,width:30,height:30,backgroundColor:'#0C0A3E',borderRadius:8,borderColor:'#0AD48B',borderWidth:3, marginRight: 5} : {width:30,height:30,backgroundColor:'#0C0A3E',borderRadius:8, marginRight: 5}}/>
                                    <TouchableOpacity  onPress={()=>this.props.method.colorOption('#7B1E7A')} style={this.props.state.selectedColor == '#7B1E7A' ? {elevation:8,width:30,height:30,backgroundColor:'#7B1E7A',borderRadius:8,borderColor:'#0AD48B',borderWidth:3, marginRight: 5} : {width:30,height:30,backgroundColor:'#7B1E7A',borderRadius:8, marginRight: 5}}/>
                                    <TouchableOpacity  onPress={()=>this.props.method.colorOption('#B33F62')} style={this.props.state.selectedColor == '#B33F62' ? {elevation:8,width:30,height:30,backgroundColor:'#B33F62',borderRadius:8,borderColor:'#0AD48B',borderWidth:3, marginRight: 5} : {width:30,height:30,backgroundColor:'#B33F62',borderRadius:8, marginRight: 5}}/>
                                    <TouchableOpacity  onPress={()=>this.props.method.colorOption('#F9564F')} style={this.props.state.selectedColor == '#F9564F' ? {elevation:8,width:30,height:30,backgroundColor:'#F9564F',borderRadius:8,borderColor:'#0AD48B',borderWidth:3, marginRight: 5} : {width:30,height:30,backgroundColor:'#F9564F',borderRadius:8, marginRight: 5}}/>
                                    <TouchableOpacity  onPress={()=>this.props.method.colorOption('#F3C677')} style={this.props.state.selectedColor == '#F3C677' ? {elevation:8,width:30,height:30,backgroundColor:'#F3C677',borderRadius:8,borderColor:'#0AD48B',borderWidth:3, marginRight: 5} : {width:30,height:30,backgroundColor:'#F3C677',borderRadius:8, marginRight: 5}}/>
                                </View>
                            </View>

                            <View style={{marginTop: 10}}>
                                <Text style={{fontSize:12, color:'#333333'}}>Size Option</Text>
                                <View style={{flexDirection:"row"}}>
                                    <TouchableHighlight underlayColor={'white'} onPress={()=>this.props.method.sizeOption('S')} style={this.props.state.selectedSize == 'S' ? {backgroundColor:'white',elevation:8,alignItems:'center',justifyContent:'center',width:30,height:30,borderRadius:8,borderColor:'#0AD48B',borderWidth:3, marginRight: 5} : {alignItems:'center',justifyContent:'center',width:30,height:30,borderRadius:8,borderWidth:1,borderColor:'black', marginRight: 5}}><Text style={{fontWeight:'bold',color:'black',fontSize:14}}>S</Text></TouchableHighlight>
                                    <TouchableHighlight underlayColor={'white'} onPress={()=>this.props.method.sizeOption('M')} style={this.props.state.selectedSize == 'M' ? {backgroundColor:'white',elevation:8,alignItems:'center',justifyContent:'center',width:30,height:30,borderRadius:8,borderColor:'#0AD48B',borderWidth:3, marginRight: 5} : {alignItems:'center',justifyContent:'center',width:30,height:30,borderRadius:8,borderWidth:1,borderColor:'black', marginRight: 5}}><Text style={{fontWeight:'bold',color:'black',fontSize:14}}>M</Text></TouchableHighlight>
                                    <TouchableHighlight underlayColor={'white'} onPress={()=>this.props.method.sizeOption('L')} style={this.props.state.selectedSize == 'L' ? {backgroundColor:'white',elevation:8,alignItems:'center',justifyContent:'center',width:30,height:30,borderRadius:8,borderColor:'#0AD48B',borderWidth:3, marginRight: 5} : {alignItems:'center',justifyContent:'center',width:30,height:30,borderRadius:8,borderWidth:1,borderColor:'black', marginRight: 5}}><Text style={{fontWeight:'bold',color:'black',fontSize:14}}>L</Text></TouchableHighlight>
                                    <TouchableHighlight underlayColor={'white'} onPress={()=>this.props.method.sizeOption('XL')} style={this.props.state.selectedSize == 'XL' ? {backgroundColor:'white',elevation:8,alignItems:'center',justifyContent:'center',width:30,height:30,borderRadius:8,borderColor:'#0AD48B',borderWidth:3, marginRight: 5} : {alignItems:'center',justifyContent:'center',width:30,height:30,borderRadius:8,borderWidth:1,borderColor:'black', marginRight: 5}}><Text style={{fontWeight:'bold',color:'black',fontSize:14}}>XL</Text></TouchableHighlight>
                                </View>
                            </View>

                            <TextInput
                                keyboardType = {"numeric"}
                                value = {""+this.props.state.buyCount}
                                placeholder = {"Count"}
                                onChangeText = {(value)=>this.props.method.productCount(value)} 
                                style={{marginTop: 10, height: 50, width: '100%', borderRadius: 4, borderWidth: 2, borderColor: "#E2E2E2", padding: 4}}
                            />

                            <Button 
                                style={{justifyContent:'center', borderRadius: 50, alignSelf: "center", marginTop: 10}}
                                width={"100%"}
                                height={50}
                                icon={({ size, color }) => (
                                    <Image
                                    source={require('../assets/images/icons8-shopping-bag-50.png')}
                                    style={{ width: 20, height: 20, tintColor: "white" }}                        
                                    />
                                )}
                                mode="contained"
                                onPress={() =>{this.props.method.onAddToCart()}}>
                                ADD TO BAG
                            </Button>

                        </View>

                </View>
            </Modal>

        </View>
        )
    }
}
