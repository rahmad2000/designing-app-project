import React, { Component } from 'react';
import { View, Dimensions,Image,ImageBackground,Text,TouchableOpacity,Modal,StatusBar} from 'react-native';
import { Button } from 'react-native-paper';
import Carousel from '../component/Carousel';

export default class IntroductionView extends Component {
    
	constructor(props) {
        super(props);
        this.state = {
            
        };
    }

    render() {
        return <View style={{ flex: 1, alignItems: 'center', backgroundColor : "#f7f8fc"}}>

            <StatusBar backgroundColor={'black'}></StatusBar> 

            {!this.props.state.modalVisible ?
            <View>

            <Carousel 
            data={this.props.state.data}
            />

            {/* <Button 
                style={{justifyContent:'center',borderRadius:50,position:'absolute',bottom:20,alignSelf:'center'}}
                width={300}
                height={50}
                // icon={({ size, color }) => (
                //     <Image
                //         source={require('../assets/images/icons8-shopping-bag-50.png')}
                //         style={{ width: 20, height: 20, tintColor: "white",marginHorizontal:-10 }}
                //         />
                //     )}
                mode="contained"
                onPress={() =>{this.props.method.setModalVisible(true);}}>
                BACK
            </Button>  */}
            
            </View>
            :

            <View/>

            }
            
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.props.state.modalVisible}
                onRequestClose={()=>{
                    this.props.method.setModalVisible(!this.state.modalVisible);
                }}>
                        <ImageBackground source={require('../assets/images/walkthrought.jpg')} style = {{width:Dimensions.get('window').width,height:Dimensions.get('window').height,justifyContent:'center',flexDirection:"row"}}>
                            <View style={{justifyContent:'space-evenly',flexDirection:'column',width:Dimensions.get('window').width,height:Dimensions.get('window').height / 1.40,alignSelf:'flex-end'}}>
                                <Text style={{fontFamily:'TTNormsMedium',alignSelf:'center',fontSize:50,color:'black'}}>scratch</Text>
                                
                                <Text style={{fontFamily:'TTNormsRegular',alignSelf:'center',fontSize:16,color:'black',textAlign:"center"}}>Make your own design and{'\n'}We sell it for you !</Text>
                                
                                <Button
                                    uppercase={false}
                                    style={{fontFamily:'TTNormsRegular',justifyContent:'center', borderRadius: 50, alignSelf: "center",elevation:7}}
                                    width={"80%"}
                                    height={50}
                                    icon={({ size, color }) => (
                                        <Image
                                        source={require('../assets/images/icons8-email-send-30.png')}
                                        style={{ width: 20, height: 20, tintColor: "white" }}                        
                                        />
                                    )}
                                    mode="contained"
                                    onPress={() =>{this.props.method.setModalVisible(false);}}>
                                    Take Walkthrought Now !
                                </Button>

                                <Text onPress={()=>this.props.method.onNextPress()} style={{fontFamily:'TTNormsRegular',alignSelf:'center',fontSize:16,color:'black'}}>Log in with account</Text>

                            </View>
                        </ImageBackground>
            </Modal>
            

            {/* <View style={{flex : 0.1, width : Dimensions.get("window").width, justifyContent : "center", paddingHorizontal : 10, alignItems : "center"}}>
                <Button
                style = {{width: Dimensions.get('window').width / 2,}}
                color ={ '#61cf63' }
                dark = { true }
                mode = {'contained'}
                label = {'Next'}
                onPress = {()=>this.props.onNextPress()}/>
            </View> */}
            
            
        </View>
    }
}