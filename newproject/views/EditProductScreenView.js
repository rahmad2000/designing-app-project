import React, { Component } from 'react'
import { Text, View, Image, Dimensions, TouchableOpacity, Modal, TextInput, Picker } from 'react-native'
import { Link } from '../systems/Config'

export default class EditProductScreenView extends Component {
    render() {
        return <View style={{flex: 1, backgroundColor: "#0AD48B", padding: 10, paddingVertical: 20}}>

            <View style={{backgroundColor: "#FFFFFF", borderRadius: 8, height: Dimensions.get('window').height / 3, overflow: "hidden", elevation: 8}}>
                <Image
                    style={{
                        height: "100%",
                        width: "100%",
                        resizeMode: "contain"
                    }} 
                    source={{uri: Link + this.props.state.productImage}}
                />
            </View>

            <TextInput
                keyboardType = {"default"}
                placeholder = {"Product Name"} 
                onChangeText = {(value)=>this.props.method.productName(value)}
                defaultValue = {this.props.state.productName}
                style={{marginTop: 8, height: 50, width: '100%', borderRadius: 4, borderWidth: 2, borderColor: "#E2E2E2", padding: 10, fontSize: 16, backgroundColor: "#FFFFFF"}}
            />
            <TextInput
                keyboardType = {"numeric"}
                placeholder = {"Product Price"}
                onChangeText = {(value)=>this.props.method.productPrice(value)}
                defaultValue = {this.props.state.productPrice} 
                style={{marginTop: 8, height: 50, width: '100%', borderRadius: 4, borderWidth: 2, borderColor: "#E2E2E2", padding: 10, fontSize: 16, backgroundColor: "#FFFFFF"}}
            />

            <View style={{marginTop: 8, borderRadius: 4, borderWidth: 2, borderColor: "#E2E2E2", backgroundColor: "#FFFFFF"}}>
                <Picker
                    style={{
                        height: 50,
                        width: "100%",
                    }}
                    mode={"dropdown"}
                    selectedValue={ this.props.state.productCategory }
                    onValueChange={(itemValue, itemIndex) => this.props.method.onCategoryChange( itemValue )}>  
                    
                    <Picker.Item label="T-Shirt" value="T-Shirt" onValueChange={this.props.state.productCategory} />
                    <Picker.Item label="Hoodie" value="Hoodie" onValueChange={this.props.state.productCategory} />
                    <Picker.Item label="Mug" value="Mug" onValueChange={this.props.state.productCategory} />
                    <Picker.Item label="Cap" value="Cap" onValueChange={this.props.state.productCategory} />
                    
                </Picker>
            </View>

            <TextInput
                keyboardType = {"default"}
                placeholder = {"Product Description"} 
                onChangeText = {(value)=>this.props.method.productDetail(value)}
                defaultValue = {this.props.state.productDetail}
                style={{marginTop: 8, height: 50, width: '100%', borderRadius: 4, borderWidth: 2, borderColor: "#E2E2E2", padding: 10, fontSize: 16, backgroundColor: "#FFFFFF"}}
            />

            <View style={{flex: 1, justifyContent: "flex-end"}}> 
                <TouchableOpacity onPress={()=>this.props.method.editProduct()}>
                    <View style={{borderRadius: 24, padding: 12, backgroundColor: "#FFFFFF", elevation: 4, justifyContent: "center", alignItems: "center"}}>
                        <Text style={{color: "#0AD48B", fontFamily: "MrEaves-Bold", fontSize: 18}}>Edit Product</Text>
                    </View>
                </TouchableOpacity>
            </View>
            
        </View>
    }
}
