import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, ScrollView, FlatList, Dimensions, RefreshControl, ActivityIndicator } from 'react-native'
import { Link } from '../systems/Config';

export default class ItemCategoryScreenView extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    render() {
        return <View style={{ flex: 1}}>
            <View style={{ flexDirection: 'row', height: 56, backgroundColor: '#FFFFFF', alignItems: 'center', justifyContent:"space-between" }}>
                <TouchableOpacity onPress={this.props.method.onBackPress}>
                    <Image source={require('../assets/images/back.png')} style={{ width: 24, height: 24, margin: 16, tintColor: '#000000' }} />
                </TouchableOpacity>
                <Text style={{ color: '#555555', fontSize: 22, fontFamily: 'MrEaves-Bold' }} >{this.props.state.category}</Text>
                <View style={{ width: 32, height: 32, marginHorizontal: 8 }}></View>
            </View>
            <ScrollView contentContainerStyle={{flexGrow: 1}} refreshControl={
                <RefreshControl 
                    onRefresh={this.props.method.onRefresh}
                    refreshing={this.props.state.refreshing}
                />
            }>
                {!this.props.state.modalVisible ? 
                    <FlatList 
                        data={this.props.state.data}
                        numColumns={2}
                        columnWrapperStyle={{ justifyContent: 'space-between' }}
                        renderItem={({ item }) => 
                        <TouchableOpacity onPress={() => this.props.onDetailPress(
                            {
                                product_id : item.product_id,
                                name_product : item.name_product,
                                price : item.price,
                                image : item.image,
                                detail : item.detail
                            })}>
                   
                            <View style={{ marginBottom: 16, overflow: 'hidden', width: Dimensions.get('window').width / 2.3, borderRadius: 7, elevation:3.6 }}>
                                <View style={{ backgroundColor: '#FFFFFF', alignItems: 'center', justifyContent: 'center', width: '100%', height: Dimensions.get('window').height / 4}}>
                                    <Image source={{uri: Link + item.image}} style={{ width: '100%', height: '100%', resizeMode: 'cover' }} />
                                </View>
                                <View style={{height: 50,backgroundColor: '#FFFFFF', paddingHorizontal:9, justifyContent:'center', paddingBottom:7}}>
                                    <Text numberOfLines={2} style={{ color: '#555555', fontSize:16.5, fontFamily:'MrEaves-Reg'}}>{item.name_product}</Text>
                                    <Text style={{ color: '#6AA84F', fontSize:12 ,fontFamily:'MrEaves-Reg'}}>Rp.{item.price}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>}
                        keyExtractor={(item, index) => item.product_id}
                        style={{ paddingHorizontal: 16, paddingTop: 16 }} 
                    />
                    :
                    <View style={{flex: 1, justifyContent: "center", marginTop: 36}}>
                        <ActivityIndicator size='large' color={'#555555'} />
                    </View>
                }
            </ScrollView>
        </View>
    }
}