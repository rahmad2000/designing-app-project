import React, { Component } from 'react'
import { Text, View, Image, Dimensions, TouchableOpacity, Modal, TextInput, Picker } from 'react-native'
import { Link } from '../systems/Config'

export default class DesignDetailScreenView extends Component {
    render() {
        return <View style={{flex: 1, backgroundColor: "#0AD48B", padding: 10, paddingVertical: 20}}>

            <Modal 
                animationType="slide"
                transparent={true}
                visible={this.props.state.modalVisible}
                onRequestClose={() => {
                    this.props.method.uploadModal(false);
                }}>
                    
                <View style={{flex: 1, backgroundColor: "rgba(0,0,0,0.3)", justifyContent: "center", alignItems: "center"}}>
                    <View style={{backgroundColor: "#FFFFFF", borderRadius: 8, padding: 12, alignItems: "center", elevation: 10}}>

                        <Text style={{fontSize: 20, fontFamily: "MrEaves-Bold"}}>Product Details</Text>
                        <TextInput
                            keyboardType = {"default"}
                            placeholder = {"Product Name"} 
                            onChangeText = {(value)=>this.props.method.productName(value)} 
                            style={{marginTop: 8, height: 50, width: 240, borderRadius: 4, borderWidth: 2, borderColor: "#E2E2E2", padding: 4}}
                        />
                        <TextInput
                            keyboardType = {"numeric"}
                            placeholder = {"Product Price"}
                            onChangeText = {(value)=>this.props.method.productPrice(value)} 
                            style={{marginTop: 8, height: 50, width: 240, borderRadius: 4, borderWidth: 2, borderColor: "#E2E2E2", padding: 4}}
                        />

                        <TextInput
                            keyboardType = {"default"}
                            placeholder = {"Product Description"}
                            onChangeText = {(value)=>this.props.method.productDescription(value)} 
                            style={{marginTop: 8, height: 50, width: 240, borderRadius: 4, borderWidth: 2, borderColor: "#E2E2E2", padding: 4}}
                        />

                        <View style={{marginTop: 8, borderRadius: 4, borderWidth: 2, borderColor: "#E2E2E2"}}>
                            <Picker
                                style={{
                                    height: 50,
                                    width: 240,
                                }}
                                mode={"dropdown"}
                                selectedValue={ this.props.state.category }
                                onValueChange={(itemValue, itemIndex) => this.props.method.onCategoryChange( itemValue )}>  
                                
                                <Picker.Item label="T-Shirt" value="T-Shirt" onValueChange={this.props.state.category} />
                                <Picker.Item label="Hoodie" value="Hoodie" onValueChange={this.props.state.category} />
                                <Picker.Item label="Mug" value="Mug" onValueChange={this.props.state.category} />
                                <Picker.Item label="Cap" value="Cap" onValueChange={this.props.state.category} />
                                
                            </Picker>
                        </View>

                        <View style={{marginTop: 8, borderRadius: 4, borderWidth: 2, borderColor: "#E2E2E2", width: 240, padding: 4}}>
                            <View style={{flexDirection:"row", justifyContent: 'space-between'}}>
                                <TouchableOpacity onPress={()=>this.props.method.sizeOption(1)} style={this.props.state.sizeS == true ? {backgroundColor:'white',elevation:8,alignItems:'center',justifyContent:'center',width:30,height:30,borderRadius:8,borderColor:'#0AD48B',borderWidth:3, marginRight: 5} : {alignItems:'center',justifyContent:'center',width:30,height:30,borderRadius:8,borderWidth:1,borderColor:'black', marginRight: 5}}><Text style={{fontWeight:'bold',color:'black',fontSize:14}}>S</Text></TouchableOpacity>
                                <TouchableOpacity onPress={()=>this.props.method.sizeOption(2)} style={this.props.state.sizeM == true ? {backgroundColor:'white',elevation:8,alignItems:'center',justifyContent:'center',width:30,height:30,borderRadius:8,borderColor:'#0AD48B',borderWidth:3, marginRight: 5} : {alignItems:'center',justifyContent:'center',width:30,height:30,borderRadius:8,borderWidth:1,borderColor:'black', marginRight: 5}}><Text style={{fontWeight:'bold',color:'black',fontSize:14}}>M</Text></TouchableOpacity>
                                <TouchableOpacity onPress={()=>this.props.method.sizeOption(3)} style={this.props.state.sizeL == true ? {backgroundColor:'white',elevation:8,alignItems:'center',justifyContent:'center',width:30,height:30,borderRadius:8,borderColor:'#0AD48B',borderWidth:3, marginRight: 5} : {alignItems:'center',justifyContent:'center',width:30,height:30,borderRadius:8,borderWidth:1,borderColor:'black', marginRight: 5}}><Text style={{fontWeight:'bold',color:'black',fontSize:14}}>L</Text></TouchableOpacity>
                                <TouchableOpacity onPress={()=>this.props.method.sizeOption(4)} style={this.props.state.sizeXL == true ? {backgroundColor:'white',elevation:8,alignItems:'center',justifyContent:'center',width:30,height:30,borderRadius:8,borderColor:'#0AD48B',borderWidth:3} : {alignItems:'center',justifyContent:'center',width:30,height:30,borderRadius:8,borderWidth:1,borderColor:'black'}}><Text style={{fontWeight:'bold',color:'black',fontSize:14}}>XL</Text></TouchableOpacity>
                            </View>
                        </View>
                       
                       <TouchableOpacity onPress={()=>this.props.method.addProduct(this.props.state.designImage)}>
                           <View style={{marginTop: 8, backgroundColor: "#0AD48B", padding: 8, paddingHorizontal: 12, borderRadius: 24, justifyContent: "center", alignItems: "center"}}>
                                <Text style={{fontFamily: "MrEaves-Bold", fontSize: 16, color: "#FFFFFF"}}>Upload</Text>
                           </View>
                       </TouchableOpacity>
                    </View>
                </View>

            </Modal>

            <View style={{backgroundColor: "#FFFFFF", borderRadius: 8, height: Dimensions.get('window').height / 2, overflow: "hidden", elevation: 8}}>
                <Image
                    style={{
                        height: "100%",
                        width: "100%",
                        resizeMode: "contain"
                    }} 
                    source={{uri: Link + this.props.state.designImage}}
                />
            </View>
            
            <Text style={{marginTop: 10, fontSize: 32, color: "#FFFFFF", fontFamily: "MrEaves-Bold"}}>"{this.props.state.designName}"</Text>
            <Text style={{fontSize: 14, color: "#FFFFFF", fontStyle: "italic"}}>By {this.props.state.designer}</Text>

            <View style={{flex: 1, justifyContent: "flex-end"}}> 
                <TouchableOpacity onPress={()=>this.props.method.uploadModal(true)}>
                    <View style={{borderRadius: 24, padding: 12, backgroundColor: "#FFFFFF", elevation: 4, justifyContent: "center", alignItems: "center"}}>
                        <Text style={{color: "#0AD48B", fontFamily: "MrEaves-Bold", fontSize: 18}}>Add To Product</Text>
                    </View>
                </TouchableOpacity>
            </View>
            
        </View>
    }
}
