import React, { Component } from 'react'
import { Text, View, Image, TextInput, FlatList, Dimensions, ScrollView, TouchableOpacity, ImageBackground, RefreshControl, ActivityIndicator, StatusBar } from 'react-native'
import { Link } from '../systems/Config';
import LinearGradient from 'react-native-linear-gradient'

export default class CartScreenView extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return <View style={{ flex: 1, backgroundColor: '#F5F5F5' }}>
            <StatusBar backgroundColor={'#0AD48B'}/>
            <View style={{ flexDirection: 'row', height: 56, alignItems: 'center',backgroundColor:'#FFFFFF',justifyContent:"space-between" }}>
                <TouchableOpacity onPress={this.props.method.openDrawer}>
                    <Image source={require('../assets/images/menu.png')} style={{ width: 24, height: 24, margin: 16, tintColor:'#0AD48B' }} />
                </TouchableOpacity>
                <Text style={{ color: '#0AD48B', fontSize: 22, fontFamily: 'MrEaves-Bold' }} >SCRATCH</Text>
                <View style={{ width: 32, height: 32, marginHorizontal: 8 }}></View>
            </View>
            <ScrollView contentContainerStyle={{flexGrow: 1}} refreshControl={
                <RefreshControl 
                    onRefresh={this.props.method.onRefresh}
                    refreshing={this.props.state.refreshing}
                />
            }>
                {!this.props.state.modalVisible ? 
                    <FlatList 
                        data={this.props.state.data}
                        renderItem={({ item }) => 
                            <View style={{ marginTop: 8, width: '100%', flexDirection: 'row' }}>
                                <View style={{ backgroundColor: '#FFFFFF', alignItems: 'center', justifyContent: 'center', width: 100, height: 100}}>
                                    <Image source={{uri: Link + item.image}} style={{ width: '100%', height: '100%', resizeMode: 'cover' }} />
                                </View>
                                <View style={{ flex: 1, height: 100, backgroundColor: '#FFFFFF', justifyContent: 'space-evenly', paddingHorizontal: 8 }}>
                                    <Text numberOfLines={2} style={{ color: '#555555', fontSize:20, fontFamily:'MrEaves-Reg', maxWidth: 160 }}>{item.name_product} [{item.count_product}]</Text>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Text style={{ color: '#555555', fontSize: 18, fontFamily:'MrEaves-Reg' }}>Size {item.size} | Colour </Text>
                                        <View style={{ elevation: 8 ,width: 20, height: 20, borderRadius: 4, backgroundColor: item.colour }}></View>
                                    </View>
                                    <Text style={{ color: '#6AA84F', fontSize: 16, fontFamily: 'MrEaves-Reg', maxWidth: '100%' }}>Rp. {item.total_price}</Text>
                                </View>
                                <View style={{ backgroundColor: '#FFFFFF', width: 56, height: 100, justifyContent: 'center', alignItems: 'center' }}>
                                    <TouchableOpacity onPress={() => this.props.method.onDelete(item.cart_id)}>
                                        <Image source={require('../assets/images/trash.png')} style={{ width: 24, height: 24, resizeMode: 'cover' }} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        }
                        keyExtractor={(item, index) => item.cart_id}
                        style={{ paddingHorizontal: 8 }}            
                    />          
                    :
                    <View style={{flex: 1, justifyContent: "center", marginTop: 36}}>
                        <ActivityIndicator size='large' color={'#555555'} />
                    </View>
                }
            </ScrollView>
            <View style={{ margin: 8 }}> 
                <TouchableOpacity onPress={()=>this.props.method.checkoutPress()}>
                    <View style={{ borderRadius: 24, padding: 12, backgroundColor: '#0AD48B', elevation: 4, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ color: '#FFFFFF', fontFamily: 'MrEaves-Bold', fontSize: 18 }}>Checkout</Text>
                    </View>
                </TouchableOpacity>
            </View> 
        </View>
    }
}