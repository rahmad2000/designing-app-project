import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, ImageBackground, ScrollView } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'

export default class CategoryScreenView extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return <View style={{ flex: 1, backgroundColor: '#F5F5F5' }}>
            <View style={{ flexDirection: 'row', height: 56, backgroundColor: '#FFFFFF', alignItems: 'center', justifyContent:"space-between" }}>
                <TouchableOpacity onPress={this.props.method.openDrawer}>
                    <Image source={require('../assets/images/menu.png')} style={{ width: 24, height: 24, margin: 16, tintColor: "#0AD48B" }} />
                </TouchableOpacity>
                <Text style={{ color: '#0AD48B', fontSize: 22, fontFamily: 'MrEaves-Bold' }} >SCRATCH</Text>
                <View style={{ width: 32, height: 32, marginHorizontal: 8 }}></View>
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{ flex: 1, padding: 12 }}>
                <View style={{ height: 160, borderRadius: 7, elevation: 3.6, overflow: "hidden" }} >
                    <ImageBackground source={require('../assets/images/tshirt.jpg')} style={{ width: '100%', height: '100%', resizeMode: 'cover', justifyContent: 'center' }}>
                        <TouchableOpacity  style={{ flex: 1 }} onPress={()=>this.props.method.onCategoryPress("T-Shirt")}>
                            <LinearGradient useAngle={true} angle={45} colors={["#43ba69", "#FFFFFF00"]} style={{flex: 1, justifyContent: "center"}}>
                                <Text style={{ color: '#FFFFFF', fontSize: 28, marginHorizontal: 16, fontFamily: "MrEaves-Bold" }}>T-SHIRT</Text>
                            </LinearGradient>
                            {/* <View style={{flex: 1, backgroundColor: "rgba(10, 212, 139, 0.4)", justifyContent: "center"}}>
                                
                            </View> */}
                        </TouchableOpacity>
                    </ImageBackground>
                </View>
                <View style={{ height: 160, marginTop: 12, borderRadius: 7, elevation: 3.6, overflow: "hidden" }} >
                    <ImageBackground source={require('../assets/images/hoodie.jpg')} style={{ width: '100%', height: '100%', resizeMode: 'cover', justifyContent: 'center' }}>
                        <TouchableOpacity  style={{ flex: 1 }} onPress={()=>this.props.method.onCategoryPress("Hoodie")}>
                            <LinearGradient useAngle={true} angle={45} colors={["#f0595e", "#FFFFFF00"]} style={{flex: 1, justifyContent: "center"}}>
                                <Text style={{ color: '#FFFFFF', fontSize: 28, marginHorizontal: 16, fontFamily: "MrEaves-Bold" }}>HOODIE</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </ImageBackground>
                </View>
                <View style={{ height: 160, marginTop: 12, borderRadius: 7, elevation: 3.6, overflow: "hidden" }} >
                    <ImageBackground source={require('../assets/images/mug.jpg')} style={{ width: '100%', height: '100%', resizeMode: 'cover', justifyContent: 'center' }}>
                        <TouchableOpacity  style={{ flex: 1 }} onPress={()=>this.props.method.onCategoryPress("Mug")}>
                            <LinearGradient useAngle={true} angle={45} colors={["#627ee3", "#FFFFFF00"]} style={{flex: 1, justifyContent: "center"}}>
                                <Text style={{ color: '#FFFFFF', fontSize: 28, marginHorizontal: 16, fontFamily: "MrEaves-Bold" }}>MUG</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </ImageBackground>
                </View>
                <View style={{ height: 160, marginTop: 12, borderRadius: 7, elevation: 3.6, overflow: "hidden" }} >
                    <ImageBackground source={require('../assets/images/cap.jpg')} style={{ width: '100%', height: '100%', resizeMode: 'cover', justifyContent: 'center' }}>
                        <TouchableOpacity  style={{ flex: 1 }} onPress={()=>this.props.method.onCategoryPress("Cap")}>
                            <LinearGradient useAngle={true} angle={45} colors={["#d453ab", "#FFFFFF00"]} style={{flex: 1, justifyContent: "center"}}>
                                <Text style={{ color: '#FFFFFF', fontSize: 28, marginHorizontal: 16, fontFamily: "MrEaves-Bold" }}>CAP</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </ImageBackground>
                </View>
            </View>
            </ScrollView>
        </View>
    }
}