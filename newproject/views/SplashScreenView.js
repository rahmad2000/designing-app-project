import React, { Component } from 'react'
import { Text, View, TouchableHighlight,Image ,Dimensions, StatusBar} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'

export default class FirstScreenView extends Component {
    render() {
        return (
           
            <View style={{
                flex: 1,
                justifyContent:'center',
                alignItems:'center',
                backgroundColor:'#e9e9e9'
            }}>
            
            <StatusBar backgroundColor={'#e9e9e9'}/>

                <View style={{
                    justifyContent:'center',
                    alignItems:'center',
                    alignContent:'center',
                    width: Dimensions.get('window').width,
                    height: Dimensions.get('window').height / 2
                   
                    }}>
                    
                    <Image
                        source={require('../assets/images/LogoScratchRedesigned.png')}
                        style={{
                            height : 200, 
                            resizeMode:'contain',
                            width : 300,
                            alignSelf:'center'
                    }}/>
                    
                </View>

            </View>
         
        )
    }
}