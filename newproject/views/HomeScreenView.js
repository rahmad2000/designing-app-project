import React, { Component } from 'react'
import { Text, View, Image, TextInput, FlatList, Dimensions, ScrollView, TouchableOpacity, ImageBackground, RefreshControl, ActivityIndicator, StatusBar } from 'react-native'
import { Link } from '../systems/Config';
import LinearGradient from 'react-native-linear-gradient'

export default class HomeScreenView extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return <View style={{ flex: 1, backgroundColor: '#F5F5F5' }}>
            <StatusBar backgroundColor={'#0AD48B'}/>
            {/* <LinearGradient colors={['#00AB6B','#00C9A7','#0AD48B']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} style={{}}> */}
            <View style={{ flexDirection: 'row', height: 56, alignItems: 'center',backgroundColor:'#FFFFFF',justifyContent:"space-between" }}>
                <TouchableOpacity onPress={this.props.method.openDrawer}>
                    <Image source={require('../assets/images/menu.png')} style={{ width: 24, height: 24, margin: 16, tintColor:'#0AD48B' }} />
                </TouchableOpacity>
                <Text style={{ color: '#0AD48B', fontSize: 22, fontFamily: 'MrEaves-Bold' }} >SCRATCH</Text>
                <View style={{ width: 32, height: 32, marginHorizontal: 8 }}></View>
            </View>
            {/* </LinearGradient> */}
            <ScrollView contentContainerStyle={{flexGrow: 1}} 
                stickyHeaderIndices={[1]}
                refreshControl={
                <RefreshControl 
                    onRefresh={this.props.method.onRefresh}
                    refreshing={this.props.state.refreshing}
                />
            }>
               
                <ImageBackground source={require('../assets/images/backhome.png')} style={{ width: '100%', height: 200, resizeMode: 'cover', justifyContent: 'flex-end'}}>
                    <View style={{height: Dimensions.get('window').height / 5, alignSelf:'flex-start', margin:16}}>
                    <Text style={{fontFamily:'MrEaves-Bold', fontSize:27, color:'#ffffff'}}>The new</Text>
                    <Text style={{fontFamily:'MrEaves-Bold', fontSize:27, color:'#ffffff'}}>Design & Online Store</Text>
                    <Text style={{fontFamily:'MrEaves-Reg', fontSize:15, color:'#ffffff', marginTop:13}}>only in</Text>
                    <Text style={{fontFamily:'MrEaves-Reg', fontSize:15, color:'#ffffff'}}>SCRATCH</Text>
                    </View>
                </ImageBackground>

                <View style={{marginTop: 10}}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center',  paddingHorizontal: 16, paddingVertical: 10, backgroundColor: "#F5F5F5"}}>
                        <TextInput
                            value={this.props.state.search}
                            onChangeText={(value)=>this.props.method.onChangeSearch(value)}
                            onSubmitEditing={()=>this.props.method.onSubmitSearch(this.props.state.search)}
                            placeholder={'Search...'} 
                            style={{ backgroundColor: '#FFFFFF', height: 40, width: Dimensions.get('window').width/1.3, padding: 0, paddingLeft: 16, borderBottomLeftRadius:50, borderTopLeftRadius:50}} />
                        <View style={{backgroundColor: '#FFFFFF', borderBottomRightRadius:50, borderTopRightRadius:50, justifyContent: "center"}}>
                            <TouchableOpacity onPress={()=>this.props.method.onSubmitSearch(this.props.state.search)} style={{ paddingHorizontal: 13, justifyContent: 'center', alignItems: 'center'}}>
                                <Image source={require('../assets/images/search.png')} style={{ width: 25, height: 25}} />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                {!this.props.state.modalVisible ? 
                    <FlatList
                        onDetailPress = {(params) => this.props.onDetailPress(params)} 
                        data={this.props.state.data}
                        numColumns={2}
                        columnWrapperStyle={{ justifyContent: 'space-between' }}
                        renderItem={({ item }) => 
                        <TouchableOpacity onPress={() => this.props.onDetailPress(
                            {
                                product_id : item.product_id,
                                name_product : item.name_product,
                                price : item.price,
                                image : item.image,
                                detail : item.detail
                            })}>
                   
                            <View style={{ marginBottom: 16, overflow: 'hidden', width: Dimensions.get('window').width / 2.3, borderRadius: 7, elevation:3.6 }}>
                                <View style={{ backgroundColor: '#FFFFFF', alignItems: 'center', justifyContent: 'center', width: '100%', height: Dimensions.get('window').height / 4}}>
                                    <Image source={{uri: Link + item.image}} style={{ width: '100%', height: '100%', resizeMode: 'cover' }} />
                                </View>
                                <View style={{height: 50,backgroundColor: '#FFFFFF', paddingHorizontal:9, justifyContent:'center', paddingBottom:7}}>
                                    <Text numberOfLines={2} style={{ color: '#555555', fontSize:16.5, fontFamily:'MrEaves-Reg'}}>{item.name_product}</Text>
                                    <Text style={{ color: '#6AA84F', fontSize:12 ,fontFamily:'MrEaves-Reg'}}>Rp.{item.price}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>}
                        keyExtractor={(item, index) => item.product_id}
                        style={{ marginTop: 10, paddingHorizontal: 16 }}                     
                    />          
                    :
                    <View style={{flex: 1, justifyContent: "center", marginTop: 36}}>
                        <ActivityIndicator size='large' color={'#555555'} />
                    </View>
                }                
            </ScrollView>
        </View>
    }
}