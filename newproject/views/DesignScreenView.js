import React, { Component } from 'react'
import { Text, View, Modal, Alert, TouchableOpacity, Dimensions, PixelRatio, TextInput, PanResponder, Animated, Image } from 'react-native'
import { TriangleColorPicker } from 'react-native-color-picker'
import { toHsv, fromHsv } from 'react-native-color-picker'
import TextDesign from '../component/TextDesign'
import ViewShot from 'react-native-view-shot'
import CameraRoll from "@react-native-community/cameraroll";
import ImageDesign from '../component/ImageDesign';
import { ScrollView } from 'react-native-gesture-handler';

export default class DesignScreenView extends Component {
    
    constructor (props) {

        super(props);
    
        this.state = {
            
        }
    }

    componentDidMount() {
        this.props.onRef(this)
    }
    componentWillUnmount() {
        this.props.onRef(undefined)
    }

    render() {

        let textDesign = this.props.state.textData.map( (data, index) => {
            return (
                <TextDesign 
                    key={data.key} 
                    color={data.color} 
                    text={data.content} 
                    fontSize={data.fontSize}
                    fontFamily={data.fontFamily}
                    zIndex={data.index}
                    onPress={() => {this.props.method.textSettingVisible(true, data.key, data.color, data.content, data.fontSize, data.index)}}/>
            )
        });

        let imageDesign = this.props.state.imageData.map( (data, index) => {
            return (
                <ImageDesign
                    key={data.key} 
                    image={data.source}
                    width={data.width}
                    height={data.height}
                    zIndex={data.index}
                    onPress={() => {this.props.method.imageSettingVisible(true, data.key, data.source, data.width, data.height, data.ratio, data.index)}}
                    />
            )
        });
        return (
            <View style={{flex: 1, backgroundColor: "#F2F2F4", justifyContent: "center"}}>

                <Modal animationType="slide"
                transparent={true}
                visible={this.props.state.colorPickerVisible}
                onRequestClose={() => {
                    this.props.method.colorPickerVisible(false);
                  }}>
                    <View style={{flex: 1, justifyContent: "flex-end", paddingHorizontal: 8}}>
                        <View style={{
                            backgroundColor: "#2E2E2E", 
                            height: Dimensions.get("window").height / 3, 
                            borderWidth: 2, borderColor: "#EBEBEB", borderBottomWidth: 0,
                            borderTopLeftRadius: 12, 
                            borderTopRightRadius: 12, 
                            justifyContent: "center", 
                            alignItems: "center", 
                            padding: 16}}>

                            <TriangleColorPicker
                                color={toHsv(this.props.state.bgColor)}
                                onColorChange={color => this.props.method.bgColorChange(fromHsv(color))}
                                style={{flex: 1, width: Dimensions.get('window').width / 2}}
                            />

                            <TouchableOpacity
                                style={{position: "absolute", top: 10, right: 10}}
                                onPress={() => {
                                this.props.method.colorPickerVisible(false);
                                }}>
                                <Image style={{width: 12, height: 12, resizeMode: "contain", tintColor: "#EBEBEB"}} source={require('../assets/images/delete.png')}></Image>
                            </TouchableOpacity>
                        </View>  
                    </View>
                </Modal>

                <Modal animationType="slide"
                transparent={true}
                visible={this.props.state.textSettingVisible}
                onRequestClose={() => {
                    this.props.method.textSettingVisible(false);
                  }}>
                    <View style={{flex: 1, justifyContent: "flex-end", paddingHorizontal: 8}}>
                        <View style={{
                            backgroundColor: "#2E2E2E", 
                            borderWidth: 2, borderColor: "#EBEBEB", borderBottomWidth: 0,
                            borderTopLeftRadius: 12, 
                            borderTopRightRadius: 12, 
                            paddingVertical: 24,
                            padding: 16, flexDirection: "row"}}>

                            <View style={{marginRight: 12}}>

                                <Text style={{color: "#FFFFFF", marginTop: 8}}>Text Fill</Text>
                                <TextInput 
                                    defaultValue={this.props.state.textValue}
                                    onChangeText={(value) => this.props.method.onChangeText(value)}
                                    style={{backgroundColor: "#FFFFFF", borderRadius: 4, width: 125, height: 35, paddingHorizontal: 4, paddingVertical: 0}}
                                    onSubmitEditing={() => this.props.method.textEdit(this.props.state.selectedText)}
                                />

                                <Text style={{color: "#FFFFFF", marginTop: 8}}>Font Size</Text>
                                <TextInput 
                                    defaultValue={""+this.props.state.textSizeValue}
                                    keyboardType={"numeric"}
                                    onChangeText={(value) => this.props.method.onChangeFontSize(value)}
                                    style={{backgroundColor: "#FFFFFF", borderRadius: 4, width: 35, height: 35, paddingHorizontal: 4, paddingVertical: 0}}
                                    onSubmitEditing={() => this.props.method.textSizeEdit(this.props.state.selectedText)}
                                />

                                <TouchableOpacity onPress={()=>this.props.method.fontSetting(true)} style={{marginTop: 8}}>
                                    <Text style={{color: "#FFFFFF"}}>Font Setting</Text>
                                </TouchableOpacity>

                                <Text style={{color: "#FFFFFF", marginTop: 8}}>Layering</Text>
                                <View style={{flexDirection: "row", alignItems: "center"}}>
                                    
                                    <TouchableOpacity onPress={() => this.props.method.layerToBack("text", this.props.state.selectedText)}>
                                        <Image style={{width: 25, height: 25, resizeMode: "contain", tintColor: "#EBEBEB"}} source={require('../assets/images/sendToBack.png')}/>
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.props.method.layerBack("text", this.props.state.selectedText)} style={{marginLeft: 12}}>
                                        <Image style={{width: 25, height: 25, resizeMode: "contain", tintColor: "#EBEBEB"}} source={require('../assets/images/toBack.png')}/>
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.props.method.layerForward("text", this.props.state.selectedText)} style={{marginLeft: 12}}>
                                        <Image style={{width: 25, height: 25, resizeMode: "contain", tintColor: "#EBEBEB"}} source={require('../assets/images/toFront.png')}/>
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={() => this.props.method.layerToFront("text", this.props.state.selectedText)} style={{marginLeft: 12}}>
                                        <Image style={{width: 25, height: 25, resizeMode: "contain", tintColor: "#EBEBEB"}} source={require('../assets/images/bringToFront.png')}/>
                                    </TouchableOpacity>
                                </View>

                                <Text style={{color: "#FFFFFF", marginTop: 8}}>Delete Text</Text>
                                <TouchableOpacity onPress={() => this.props.method.deleteText(this.props.state.selectedText)}>
                                    <Image style={{width: 25, height: 25, resizeMode: "contain", tintColor: "#EBEBEB"}} source={require('../assets/images/trash.png')}/>
                                </TouchableOpacity>
                            
                            </View>

                            <TriangleColorPicker
                                color={toHsv(this.props.state.textColor)}
                                onColorChange={color => this.props.method.textColorChange(fromHsv(color), this.props.state.selectedText)}
                                style={{flex: 1}}
                            />
                            
                            <TouchableOpacity
                                style={{position: "absolute", top: 10, right: 10}}
                                onPress={() => {
                                this.props.method.textSettingVisible(false);
                                }}>
                                <Image style={{width: 12, height: 12, resizeMode: "contain", tintColor: "#EBEBEB"}} source={require('../assets/images/delete.png')}/>
                            </TouchableOpacity>
                        </View>  
                    </View>
                </Modal>

                <Modal animationType="slide"
                transparent={true}
                visible={this.props.state.imageSettingVisible}
                onRequestClose={() => {
                    this.props.method.imageSettingVisible(false);
                  }}>
                    <View style={{flex: 1, justifyContent: "flex-end", paddingHorizontal: 8}}>
                        <View style={{
                            backgroundColor: "#2E2E2E", 
                            borderWidth: 2, borderColor: "#EBEBEB", borderBottomWidth: 0,
                            borderTopLeftRadius: 12, 
                            borderTopRightRadius: 12, 
                            padding: 16, paddingVertical: 24}}>

                            <View style={{flexDirection: "row", alignItems: "center"}}>
                                <Text style={{color: "#FFFFFF", marginTop: 12}}>Size</Text>

                                <View style={{alignItems: "center", marginLeft: 12}}>

                                    <Text style={{fontSize: 12, color: "#FFFFFF"}}>Width</Text>
                                    <TextInput 
                                        value={""+this.props.state.selectedWidth}
                                        keyboardType={"numeric"}
                                        onChangeText={(value) => this.props.method.onChangeImageWidth(value)}
                                        style={{backgroundColor: "#FFFFFF", borderRadius: 4, width: 50, height: 30, paddingHorizontal: 4, paddingVertical: 0}}
                                        onSubmitEditing={() => this.props.method.imageWidthEdit(this.props.state.selectedImage)}
                                    />
                                    
                                </View>
                                
                                <View style={{alignItems: "center", marginLeft: 12}}>

                                    <Text style={{fontSize: 12, color: "#FFFFFF"}}>Height</Text>
                                    <TextInput 
                                        value={""+this.props.state.selectedHeight}
                                        keyboardType={"numeric"}
                                        onChangeText={(value) => this.props.method.onChangeImageHeight(value)}
                                        style={{backgroundColor: "#FFFFFF", borderRadius: 4, width: 50, height: 30, paddingHorizontal: 4, paddingVertical: 0}}
                                        onSubmitEditing={() => this.props.method.imageHeightEdit(this.props.state.selectedImage)}
                                    />
                                   
                                </View>
                                
                            </View>

                            <View style={{flexDirection: "row", marginTop: 12, alignItems: "center"}}>
                                <Text style={{color: "#FFFFFF"}}>Layering</Text>
                                <TouchableOpacity onPress={() => this.props.method.layerToBack("image", this.props.state.selectedImage)} style={{marginLeft: 12}}>
                                    <Image style={{width: 25, height: 25, resizeMode: "contain", tintColor: "#EBEBEB"}} source={require('../assets/images/sendToBack.png')}/>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.props.method.layerBack("image", this.props.state.selectedImage)} style={{marginLeft: 12}}>
                                    <Image style={{width: 25, height: 25, resizeMode: "contain", tintColor: "#EBEBEB"}} source={require('../assets/images/toBack.png')}/>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.props.method.layerForward("image", this.props.state.selectedImage)} style={{marginLeft: 12}}>
                                    <Image style={{width: 25, height: 25, resizeMode: "contain", tintColor: "#EBEBEB"}} source={require('../assets/images/toFront.png')}/>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.props.method.layerToFront("image", this.props.state.selectedImage)} style={{marginLeft: 12}}>
                                    <Image style={{width: 25, height: 25, resizeMode: "contain", tintColor: "#EBEBEB"}} source={require('../assets/images/bringToFront.png')}/>
                                </TouchableOpacity>
                            </View>

                            <View style={{flexDirection: "row", marginTop: 12}}>
                                <Text style={{color: "#FFFFFF"}}>Delete Image</Text>
                                <TouchableOpacity onPress={() => this.props.method.deleteImage(this.props.state.selectedImage)}>
                                    <Image style={{marginLeft: 12, width: 25, height: 25, resizeMode: "contain", tintColor: "#EBEBEB"}} source={require('../assets/images/trash.png')}/>
                                </TouchableOpacity>
                            </View>

                            <TouchableOpacity
                                style={{position: "absolute", top: 10, right: 10}}
                                onPress={() => {
                                this.props.method.imageSettingVisible(false);
                                }}>
                                <Image style={{width: 12, height: 12, resizeMode: "contain", tintColor: "#EBEBEB"}} source={require('../assets/images/delete.png')}/>
                            </TouchableOpacity>
                        </View>  
                    </View>
                </Modal>

                <Modal 
                    animationType="slide"
                    transparent={true}
                    visible={this.props.state.fontSetVisible}
                    onRequestClose={() => {
                        this.props.method.fontSetting(false);
                    }}>
                    <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                        <View style={{width: "80%", backgroundColor: "#333333", borderRadius: 4, overflow: "hidden"}}>
                            <View style={{flexDirection: "row", justifyContent: "space-between", alignItems: "center", padding: 8}}>
                                <Text style={{color: "#FFFFFF", padding: 8}}>Change Font</Text>

                                <TouchableOpacity 
                                    style={{borderRadius: 4, backgroundColor: "#18d928", padding: 6}}
                                    onPress={()=>this.props.method.saveFont(this.props.state.selectedText)}>
                                        <View style={{height: 15, justifyContent: "center"}}>
                                            <Text style={{color: "#FFFFFF"}}>Save</Text>
                                        </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{backgroundColor: "#FFFFFF", height: 45, borderRadius: 4, padding: 8, margin: 8, marginTop: 0, alignItems: "center", justifyContent: "center"}}>
                                <Text style={{fontFamily: this.props.state.selectedFontFamily}}>{this.props.state.textValue}</Text>
                            </View>
                            <View style={{height: 125}}>
                                <ScrollView>
                                    <View style={{borderBottomColor: "#FFFFFF", borderBottomWidth: 1}}></View>
                                        <TouchableOpacity onPress={()=>this.props.method.setSelectedFont("boulevard")} style={{padding: 12, justifyContent: "center"}}>
                                            <Text style={{fontFamily: "boulevard", color: "#FFFFFF"}}>Boulevard</Text>
                                        </TouchableOpacity>
                                    <View style={{borderBottomColor: "#FFFFFF", borderBottomWidth: 1}}></View>
                                        <TouchableOpacity onPress={()=>this.props.method.setSelectedFont("monofonto")} style={{padding: 12, justifyContent: "center"}}>
                                            <Text style={{fontFamily: "monofonto", color: "#FFFFFF"}}>Monofonto</Text>
                                        </TouchableOpacity>
                                    <View style={{borderBottomColor: "#FFFFFF", borderBottomWidth: 1}}></View>
                                        <TouchableOpacity onPress={()=>this.props.method.setSelectedFont("jackInput")} style={{padding: 12, justifyContent: "center"}}>
                                            <Text style={{fontFamily: "jackInput", color: "#FFFFFF"}}>jackInput</Text>
                                        </TouchableOpacity>
                                    <View style={{borderBottomColor: "#FFFFFF", borderBottomWidth: 1}}></View>
                                        <TouchableOpacity onPress={()=>this.props.method.setSelectedFont("TheBredan")} style={{padding: 12, justifyContent: "center"}}>
                                            <Text style={{fontFamily: "TheBredan", color: "#FFFFFF"}}>The Bredan</Text>
                                        </TouchableOpacity>
                                    <View style={{borderBottomColor: "#FFFFFF", borderBottomWidth: 1}}></View>
                                        <TouchableOpacity onPress={()=>this.props.method.setSelectedFont("Poppins-Regular")} style={{padding: 12, justifyContent: "center"}}>
                                            <Text style={{fontFamily: "Poppins-Regular", color: "#FFFFFF"}}>Poppins Regular</Text>
                                        </TouchableOpacity>
                                    <View style={{borderBottomColor: "#FFFFFF", borderBottomWidth: 1}}></View>
                                        <TouchableOpacity onPress={()=>this.props.method.setSelectedFont("Poppins-Bold")} style={{padding: 12, justifyContent: "center"}}>
                                            <Text style={{fontFamily: "Poppins-Bold", color: "#FFFFFF"}}>Poppins Bold</Text>
                                        </TouchableOpacity>
                                </ScrollView>
                            </View>
                            
                        </View>
                    </View>
                    
                </Modal>


                <Modal 
                    animationType="slide"
                    transparent={true}
                    visible={this.props.state.imagePickModal}
                    onRequestClose={() => {
                        this.props.method.imagePickModal(false);
                    }}>
                    <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                        <View style={{width: "80%", backgroundColor: "#333333", borderRadius: 4, overflow: "hidden"}}>
                            <Text style={{textAlign: "center", color: "#FFFFFF", padding: 8}}>Pick image from</Text>
                            <View style={{borderBottomColor: "#FFFFFF", borderBottomWidth: 1}}></View>
                            <TouchableOpacity onPress={()=> this.props.method.imagePicker("gallery")} style={{padding: 12, justifyContent: "center"}}>
                                <Text style={{color: "#FFFFFF"}}>Gallery</Text>
                            </TouchableOpacity>
                            <View style={{borderBottomColor: "#FFFFFF", borderBottomWidth: 1}}></View>
                            <TouchableOpacity onPress={()=> this.props.method.imagePicker("camera")} style={{padding: 12, justifyContent: "center"}}>
                                <Text style={{color: "#FFFFFF"}}>Camera</Text>
                            </TouchableOpacity>
                            <View style={{borderBottomColor: "#FFFFFF", borderBottomWidth: 1}}></View>
                            <TouchableOpacity onPress={()=> this.props.method.imagePickModal(false)} style={{padding: 8, justifyContent: "center", alignItems: "center", backgroundColor: "#C9C9C9"}}>
                                <Text style={{color: "red"}}>Close</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                    
                </Modal>

                <TouchableOpacity 
                    style={{height: 35, width: 35, borderRadius: 35, position: "absolute", top: 20, left: 20, zIndex: 2}}
                    onPress={()=> this.props.method.setFullscreen()}>
                        <Image source= {this.props.state.fullscreen ? require('../assets/images/fullscreen-exit.png') : require('../assets/images/fullscreen.png')}
                            style={{height: 35, width: 35, resizeMode: "contain", tintColor: "#2E2E2E"}}/>
                </TouchableOpacity>

                <TouchableOpacity 
                    style={{position: "absolute", bottom: 20, right: 10, zIndex: 2, backgroundColor: "#0AD48B", padding: 10, borderRadius: 4}}
                    onPress={()=> this.props.method.nameVisible(true)}>
                        <View style={{height: 25}}>
                            <Text style={{color: "#FFFFFF"}}>Save</Text>
                        </View>
                </TouchableOpacity>

                {!this.props.state.fullscreen ? 
                    <View style={{flexDirection: "row", alignItems: "center", position: "absolute", zIndex: 2, top: 10, right: 10, backgroundColor: "#2E2E2E", padding: 10, borderRadius: 4}}>
                        <TouchableOpacity 
                            style={{height: 25, width: 25, borderRadius: 25, marginRight: 20}}
                            onPress={() => this.props.method.imagePickModal(true)}>
                                <Image source={require('../assets/images/image.png')}
                                    style={{height: 25, width: 25, resizeMode: "contain", tintColor: "#EBEBEB"}}/>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            style={{height: 25, width: 25, borderRadius: 25, marginRight: 20}} 
                            onPress={() => this.props.method.colorPickerVisible(true)}>
                                <Image source={require('../assets/images/paint.png')}
                                    style={{height: 25, width: 25, resizeMode: "contain", tintColor: "#EBEBEB"}}/>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            style={{height: 25, width: 25, borderRadius: 25}} 
                            onPress={() => this.props.method.addText()} >
                                <Image source={require('../assets/images/text.png')}
                                    style={{height: 25, width: 25, resizeMode: "contain", tintColor: "#EBEBEB"}}/>
                        </TouchableOpacity>
                    </View>
                    :
                    <View style={{position: "absolute"}}></View>
                }
                <ViewShot ref="viewShot">
                <View style={{justifyContent: "center", 
                    alignItems: "center", 
                    overflow: "hidden", 
                    backgroundColor: this.props.state.bgColor,
                    height: Dimensions.get('window').height,
                    width: Dimensions.get('window').width}}>
                    {imageDesign}
                    {textDesign}
                </View>
                </ViewShot>

                <Modal animationType="fade"
                transparent={true}
                visible={this.props.state.nameVisible}
                onRequestClose={() => {
                    this.props.method.nameVisible(false);
                  }}>
                    <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                        <View style={{backgroundColor: "#2E2E2E",
                            borderWidth: 2, borderColor: "#EBEBEB",
                            borderRadius: 12,
                            padding: 16}}
                        >
                            <View style={{flexDirection: "row", alignItems: 'center'}}>
                                <Text style={{color: "#FFFFFF"}}>Name</Text>
                                <TextInput 
                                    onChangeText={(value) => this.props.method.onChangeName(value)}
                                    style={{backgroundColor: "#FFFFFF", borderRadius: 4, width: 150, height: 35, marginLeft: 12, paddingVertical: 0, paddingHorizontal: 8}}
                                />
                            </View>
                            <View style={{flexDirection: "row", marginTop: 12, justifyContent: 'space-between'}}>
                                <TouchableOpacity 
                                    style={{backgroundColor: "#0AD48B", paddingVertical: 10, paddingHorizontal: 20, borderRadius: 4, marginRight: 12}}
                                    onPress={()=> this.props.method.nameVisible(false)}>
                                        <View style={{height: 25}}>
                                            <Text style={{color: "#FFFFFF"}}>Cancel</Text>
                                        </View>
                                </TouchableOpacity>
                                <TouchableOpacity 
                                    style={{backgroundColor: "#0AD48B", paddingVertical: 10, paddingHorizontal: 20, borderRadius: 4}}
                                    onPress={()=> this.props.method.saveImage()}>
                                        <View style={{height: 25}}>
                                            <Text style={{color: "#FFFFFF"}}>Save</Text>
                                        </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>

            </View>
        )
    }
}