import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, FlatList, TextInput, ActivityIndicator, RefreshControl } from 'react-native'
import { Link } from '../systems/Config';

export default class CheckoutScreenView extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    render() {
        return <View style={{ flex: 1, backgroundColor: '#0AD48B', padding: 16 }}>
            {!this.props.state.modalVisible ? 
                <FlatList 
                    showsVerticalScrollIndicator={false}
                    data={this.props.state.data}
                    renderItem={({ item }) => 
                        <View style={{ marginVertical: 4, width: '100%', flexDirection: 'row' }}>
                            <View style={{ backgroundColor: '#FFFFFF', alignItems: 'center', justifyContent: 'center', width: 100, height: 100}}>
                                <Image source={{uri: Link + item.image}} style={{ width: '100%', height: '100%', resizeMode: 'cover' }} />
                            </View>
                            <View style={{ flex: 1, height: 100, backgroundColor: '#FFFFFF', justifyContent: 'space-evenly', paddingHorizontal: 8 }}>
                                <Text numberOfLines={2} style={{ color: '#555555', fontSize:20, fontFamily:'MrEaves-Reg', maxWidth: 160 }}>{item.name_product} [{item.count_product}]</Text>
                                <Text style={{ color: '#6AA84F', fontSize: 16, fontFamily: 'MrEaves-Reg', maxWidth: '100%' }}>Rp. {item.total_price}</Text>
                            </View>
                        </View>
                    }
                    keyExtractor={(item, index) => item.cart_id}  
                    style={{backgroundColor: '#F5F5F5', paddingHorizontal: 8, borderRadius: 8 }}      
                />          
                :
                <View style={{flex: 1, justifyContent: "center", backgroundColor: '#FFFFFF', borderRadius: 8}}>
                    <ActivityIndicator size='large' color={'#555555'} />
                </View>
            }
            <View style={{ marginTop: 16 }}>
                <TextInput
                    multiline = {true}
                    keyboardType = {'default'}
                    placeholder = {'Address'} 
                    onChangeText = {(value)=>this.props.method.address(value)}
                    style={{ height: 50, width: '100%', borderRadius: 4, borderWidth: 2, borderColor: '#E2E2E2', padding: 10, fontSize: 16, backgroundColor: '#FFFFFF' }}
                />
                <Text style={{ marginTop: 10, fontSize: 28, color: '#FFFFFF', fontFamily: 'MrEaves-Bold' }}>Total Price : Rp. {this.props.state.price}</Text>
                <Text style={{ marginBottom: 10, fontSize: 14, color: '#FFFFFF', fontStyle: 'italic'}}>{this.props.state.date}</Text>
                <TouchableOpacity onPress={()=>this.props.method.transaction()}>
                    <View style={{ borderRadius: 24, padding: 12, backgroundColor: '#FFFFFF', elevation: 4, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ color: '#0AD48B', fontFamily: 'MrEaves-Bold', fontSize: 18 }}>Transaction</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    }
}