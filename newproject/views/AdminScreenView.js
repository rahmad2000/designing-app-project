import React, { Component } from 'react'
import { Text, View, Image, Dimensions, TouchableOpacity, StatusBar } from 'react-native'

export default class AdminScreenView extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return <View style={{ flex: 1, backgroundColor: '#F5F5F5' }}>
            <StatusBar backgroundColor={'#0AD48B'}/>
            <View style={{ flexDirection: 'row', height: 56, backgroundColor: '#FFFFFF', alignItems: 'center', justifyContent: 'space-between', elevation: 7 }}>
                <View style={{ width: 32, height: 32, marginHorizontal: 8 }}></View>
                <Text style={{ color: '#0AD48B', fontSize: 22, fontFamily: 'MrEaves-Bold' }} >SCRATCH</Text>
                <TouchableOpacity onPress={this.props.method.logout}>
                    <Image source={require('../assets/images/logout.png')} style={{ width: 24, height: 24, margin: 16, tintColor: "#0AD48B" }} />
                </TouchableOpacity>
            </View>

            <View style={{flexDirection: 'row', justifyContent: 'center', padding: 10}}>
                <View style={{flex: 1, marginRight: 10}}>
                    <TouchableOpacity activeOpacity={0.5} onPress={()=> this.props.method.designListPress()}>
                        <View style={{height: Dimensions.get('window').height / 4, backgroundColor: "#0AD48B", borderRadius: 8, justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{fontFamily: 'MrEaves-Bold', color: '#FFFFFF', fontSize: 18}}>Design List</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={{flex: 1}}>
                    <TouchableOpacity activeOpacity={0.5} onPress={()=> this.props.method.orderListPress()}>
                        <View style={{height: Dimensions.get('window').height / 4, backgroundColor: "#0AD48B", borderRadius: 8, justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{fontFamily: 'MrEaves-Bold', color: '#FFFFFF', fontSize: 18}}>Order List</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={{paddingHorizontal: 10}}>
                <TouchableOpacity activeOpacity={0.5} onPress={()=> this.props.method.productListPress()}>
                    <View style={{width: '100%', height: Dimensions.get('window').height / 4, backgroundColor: "#0AD48B", borderRadius: 8, justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontFamily: 'MrEaves-Bold', color: '#FFFFFF', fontSize: 18}}>Uploaded Product</Text>
                    </View>
                </TouchableOpacity>
            </View>
        
        </View>
    }
}