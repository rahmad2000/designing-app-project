import React, {Component} from 'react';
import { View, Dimensions } from 'react-native';
import { HelperText, TextInput } from 'react-native-paper';

export default class PaperTextInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }


    render(){
        return (
            <View>
                <TextInput
                    style={[{
                        width : Dimensions.get('window').width - 72,
                        
                    },this.props.style]}
                    theme={{
                        colors :{
                            primary : "#0AD48B"
                        }
                    }}
                    selectionColor={this.props.selectionColor}
                    mode={this.props.mode}
                    placeholder={this.props.placeholder}
                    label={this.props.label}
                    value={this.props.value}
                    onChangeText={this.props.onChangeText}
                    keyboardType={this.props.type}
                    secureTextEntry={this.props.secureTextEntry}
                    multiline={this.props.multiline}
                />
            </View>
        
        );
  }
}