import React, { Component } from 'react';
import { View, Image, Text, PanResponder, Animated, TouchableWithoutFeedback } from 'react-native';

export default class ImageDesign extends Component {
    constructor( props ) {
        super( props );
        this.state = {
            pan: new Animated.ValueXY(),
        };
    }

    componentWillMount = () => {
        this._panResponder = PanResponder.create({
            onMoveShouldSetResponderCapture: () => true,
            onMoveShouldSetPanResponderCapture: () => true,
        
            onPanResponderGrant: (e, gestureState) => {
                this.state.pan.setOffset({x: this.state.pan.x._value, y: this.state.pan.y._value});
                this.state.pan.setValue({x: 0, y: 0});
            },
        
            onPanResponderMove: (e, gestureState) => {
                Animated.event([null, {dx: this.state.pan.x, dy: this.state.pan.y},])(e, gestureState)
            },
            onPanResponderRelease: (e, gestureState) => {
                this.state.pan.flattenOffset();
            }
        });
    }

    render() {

        let { pan } = this.state;

        let [translateX, translateY] = [pan.x, pan.y];

        let rotate = '0deg';

        let imageStyle = {transform: [{translateX}, {translateY}, {rotate}]};

        return (
        <Animated.View style={[imageStyle, {position: "absolute", zIndex: this.props.zIndex}]} {...this._panResponder.panHandlers}>
            <TouchableWithoutFeedback onPress={this.props.onPress}>
                {/* <Image style={{width: "100%", height: "100%", resizeMode: "contain"}} source={{uri : this.props.image}}/> */}
                {/* <Text>{this.props.image}</Text> */}
                <Image style={{width: this.props.width, height: this.props.height, resizeMode: "contain"}} source={{uri : this.props.image}}/>
            </TouchableWithoutFeedback>
        </Animated.View>
        
        
        )
    }
}