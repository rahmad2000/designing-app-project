import React, { Component } from 'react';
import { Button } from 'react-native-paper';

export default class PaperButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return(
            <Button 
                background={this.props.background}
                style={this.props.style}
                dark={this.props.dark}
                color={this.props.color}
                icon={this.props.icon} 
                mode={this.props.mode} 
                onPress={() => this.props.onPress()}>
                {this.props.label}
            </Button>
        );
    }
}