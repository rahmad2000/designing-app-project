import React, {Component} from 'react';
import {ImageBackground, View, Text, Image, Dimensions,StyleSheet } from 'react-native';
import Carousel, { Pagination,ParallaxImage} from 'react-native-snap-carousel';

const { width: screenWidth } = Dimensions.get('window')


export default class Parallax extends Component {

    constructor(props){
        super(props);
        this.state = {
           activeSlide: 0,
      
        };
    }

    get pagination () {
        const { entries, activeSlide } = this.state;
        return (
            <Pagination
            
              dotsLength={this.props.data.length}
              activeDotIndex={activeSlide}
              containerStyle={[{backgroundColor: 'transparent',position:"absolute" },this.props.containerStyle]}
              dotStyle={{
                  width: 10,
                  height: 10,
                  borderRadius: 5,
                  marginHorizontal: 3,
                  backgroundColor: 'rgba(0, 0, 0, 0.92)'
              }}
              inactiveDotStyle={{
                width: 8,
                height: 8,
                borderRadius: 5,
                marginHorizontal: 3,
                backgroundColor: 'rgba(0, 0, 0, 0.50)'}}
              inactiveDotOpacity={0.4}
              inactiveDotScale={0.6}
            />
        );
    }
   

    _renderItem ({item, index}, parallaxProps) {
        return (
          
            <View style={{width : "100%", height: "100%", justifyContent : "center", alignItems : "center"}}>
                <View style={{width :Dimensions.get('window').width, height : Dimensions.get('window').height,backgroundColor:'yellow'}}>
                   
                    <ParallaxImage
                        source={ item.thumbnail }
                        containerStyle={styles.imageContainer}
                        style={styles.image}
                        parallaxFactor={0.6}
                        {...parallaxProps}
                    />
                </View>
                {
                item.id == "1" ?
                <View style=
                {{justifyContent: 'center',
                  alignItems: 'flex-end',
                  backgroundColor:'black',
                  width: Dimensions.get('window').width/1.05,
                  position:'absolute',
                  top: 10,
                  height:Dimensions.get('window').height/3,
                  borderBottomLeftRadius: 200,
                  borderBottomRightRadius:10,
                  borderTopLeftRadius:10,
                  borderTopRightRadius: 10}}>
                    <Text style={{marginRight:25,fontSize: 28,color:'white',fontFamily:'MontserratBold'}}>{item.title}</Text>
                    <Text style={{marginRight:25,maxWidth: "75%", fontSize: 16, color: 'white', textAlign : "right",fontFamily:'robotoregular'}}>{item.description}</Text>
                </View>
                :
                item.id == "2" ?
                <View style={{
                justifyContent: 'center',
                alignItems: 'flex-start',
                backgroundColor:'black',
                width: Dimensions.get('window').width/1.05,
                position:'absolute',
                top: 10,
                height:Dimensions.get('window').height/3,
                borderBottomLeftRadius: 10,
                borderBottomRightRadius:200,
                borderTopLeftRadius:10,
                borderTopRightRadius: 10}}>
                    <Text style={{marginLeft:25,fontSize: 28,color:'white',fontFamily:'MontserratBold'}}>{item.title}</Text>
                    <Text style={{marginLeft:25,maxWidth: "75%", fontSize: 16, color: 'white', textAlign : "left",fontFamily:'robotoregular'}}>{item.description}</Text>
                </View>
                
                :
                item.id == "3" ?
                <View style={{
                justifyContent: 'center',
                alignItems: 'flex-start',
                backgroundColor:'black',
                width: Dimensions.get('window').width/1.05,
                position:'absolute',
                bottom: 10,
                height:Dimensions.get('window').height/3,
                borderBottomLeftRadius: 10,
                borderBottomRightRadius:10,
                borderTopLeftRadius:10,
                borderTopRightRadius: 200}}>
                    <Text style={{marginLeft:25,fontSize: 28,color:'white',fontFamily:'MontserratBold'}}>{item.title}</Text>
                    <Text style={{marginLeft:25,maxWidth: "75%", fontSize: 16, color: 'white', textAlign : "left",fontFamily:'robotoregular'}}>{item.description}</Text>
                </View>
                :
                item.id == "4" ?
                <View style={{
                justifyContent: 'center',
                alignItems: 'flex-end',
                backgroundColor:'black',
                width: Dimensions.get('window').width/1.05,
                position:'absolute',
                bottom: 10,
                height:Dimensions.get('window').height/3,
                borderBottomLeftRadius: 10,
                borderBottomRightRadius:10,
                borderTopLeftRadius:200,
                borderTopRightRadius: 10}}>
                    <Text style={{marginRight:25,fontSize: 28,color:'white',fontFamily:'MontserratBold'}}>{item.title}</Text>
                    <Text style={{marginRight:25,maxWidth: "75%", fontSize: 16, color: 'white', textAlign : "right",fontFamily:'robotoregular'}}>{item.description}</Text>
                </View>
                :
                <View></View>
                }
            </View>
        );
    }

    render () {
        return (
            <View style={[{justifyContent:'center',alignItems:'center'}, this.props.style]}>
            
            <Carousel
            autoplay = {false}
            autoplayInterval = {2000}
            loop ={false}
            layout = {'default'}
            ref={(c) => { this._carousel = c; }}
            data={this.props.data}
            renderItem={this._renderItem}
            sliderWidth={Dimensions.get("window").width}
            itemWidth={Dimensions.get("window").width}
            sliderHeight={Dimensions.get("window").height}
            itemHeight={Dimensions.get("window").height}
            onSnapToItem={(index) => this.setState({ activeSlide: index }) }
            hasParallaxImages={true}
            />
            {/* {this.pagination} */}
            </View>
        );
    }
}

const styles = StyleSheet.create({
  
  imageContainer: {
    flex: 1,
    backgroundColor: 'white',
    

  },
  image: {
    ...StyleSheet.absoluteFillObject,
    resizeMode: 'contain',
  },
})

